'use strict';
const mongoose = require('mongoose');
const log = require('./log')(module);
const crypto = require('crypto');
const options = {promiseLibrary: require('bluebird'), server: { socketOptions: { keepAlive: 1 } }};

const roulette = require('./files/roulette.json');
const quests = require('./files/quest.json');

const Types = require('./schemas/schema');
const User = Types.user;
const Advertiser = Types.advertiser;
const Administrator = Types.administrator;
const Token = Types.tokens;
const Invite = Types.invite;
const Finance = Types.finances;
const Answer = Types.answer;
const Event = Types.event;
const Timer = Types.timer;
const Lottery = Types.lottery;
const Victory = Types.victory;
const Checkout = Types.checkout;
const MoneyRequest = Types.money_request;
const LotteryGame = Types.lottery_game;
const Images = Types.image;
const Email = Types.email;

let mailer;

//Создание пользователя
const createUser = function (args, callback) {
    let newUser;
    if (args.type === 'user') {
        newUser = new User({
            accountID: args.accountID,
            invite: args.invite,
            referral: args.referral,
            country: args.country,
            city: args.city,
            email: args.email,
            password: args.password
        });
        if (args.referral) {
            newUser.Pr = 100;
        }
    } else if (args.type === 'advertiser') {
        newUser = new Advertiser({
            accountID: args.accountID,
            invite: args.invite,
            referral: args.referral,
            country: args.country,
            city: args.city,
            email: args.email,
            password: args.password,

            companyName: args.companyName,
            sphereOfActivity: args.sphereOfActivity,
            FIO: args.FIO,

            questionText: args.questionText,
            answerText: args.answerText,
            answerSite: args.answerSite,

            bankName: args.bankName,
            INN: args.INN,
            KPP: args.KPP,
            BIK: args.BIK,
            bankAccount: args.bankAccount,
            corAccount: args.corAccount
        });

        let quest = quests.userQuests[Math.floor(Math.random() * quests.userQuests.length)];
        newUser.VipByGameAward.type = quest.awardType;
        newUser.VipByGameAward.value = quest.awardValue;

        if (args.referral) {
            newUser.packetType = 'Тестовый';
            addTimer(newUser.id, 'prolongation', 3 * 24 * 3600 * 1000);
            newUser.activePacket = true;
        }
    } else if (args.type === 'administrator') {
        newUser = new Administrator({
            accountID: args.accountID,
            invite: args.invite,
            referral: args.referral,
            country: args.country,
            city: args.city,
            email: args.email,
            password: args.password
        });
    }
    else {
        return callback('Неверный тип');
    }

    newUser.save(function (err) {
        if (err) {
            log.log('error', 'createUser, first save: ' + err);
            return callback('Внутренняя ошибка сервера');
        }

        if (args.type === 'advertiser') {
            Finance.findOneAndUpdate({}, {$inc: {operationNumber: 1}}, function (err, finance) {
                if (err) {
                    log.log('error', 'createUser, advertiser save: ' + err);
                    return callback('Внутренняя ошибка сервера');
                }
                if (!finance) {
                    return callback('Внутренняя ошибка сервера');
                }

                let checkout = new Checkout({
                    advertiserId: newUser.id,
                    cost: finance.startPacket.cost,
                    checkoutNumber: finance.operationNumber,
                    createdAt: Date.now()
                });
                checkout.options.push('startPacket');
                checkout.save(function (err) {
                    if (err) {
                        log.log('error', 'createUser, checkout save: ' + err);
                        return callback('Внутренняя ошибка сервера');
                    }
                    mailer.sendMail({
                        theme: 'paid',
                        address: newUser.email,
                        user: newUser,
                        subject: 'Запрос на оплату',

                        operationNumber: finance.operationNumber,
                        companyName: finance.companyName,
                        services: [{
                            packetValue: 1,
                            packetName: finance.startPacket.name,
                            packetPrice: finance.startPacket.cost
                        }],
                        totalSum: finance.startPacket.cost
                    }, function (err) {
                        if (err) {
                            return log.log('error', 'createUser, send paid: ' + 'Ошибка при отправке сообщения');
                        }
                    });
                    mailer.sendMail({
                        theme: 'requisites',
                        user: newUser,
                        address: newUser.email,
                        subject: 'Ваши реквизиты'
                    }, function (err) {
                        if (err) {
                            return callback('Ошибка при отправке сообщения');
                        }
                        callback(null, newUser);
                    });
                });
            });
        } else {
            mailer.sendMail({
                theme: 'requisites',
                user: newUser,
                address: newUser.email,
                subject: 'Ваши реквизиты'
            }, function (err) {
                if (err) {
                    return callback('Ошибка при отправке сообщения');
                }
                callback(null, newUser);
            });
        }
    });
};
//Изменение числовых параметров пользователя
const addPoints = function (user, type, points, game) {
    if (user[type] || user[type] === 0) {
        let query = {};
        query[type] = points;
        User.findByIdAndUpdate(user.id, {$inc: query}, {new: true}, function (err, user) {
            if (err) {
                return log.log('error', 'addPoints: ' + err);
            }
            checkOnStar(user, type, points, game);
        });
    }
};
//Создание события
const addEvent = function (userId, eventType) {
    let event = new Event({
        type: eventType,
        userId: userId,
        date: Date.now()
    });
    event.save(function (err) {
        if (err) {
            log.log('error', 'addEvent: ' + err);
        }
    });
};
// Смена животного, кроме БИО(заморочка с событием)
const changeAnimal = function (user, animal) {
    if (animal !== 'hamster') {
        user.hamsterNumber = 0;
        if (animal == 'squirrel') {
            user.squirrelNumber++;
            user.Al = 0;
        }
        user.animalNumber = 3;
        user.Cf += 3;
    }
    user.activeAnimal = animal;
    user.save(function (err) {
        if(err){
            log.log('error', 'changeAnimal: ' + err);
        }
    });
    addEvent(user.id, animal);
};
//Проверки на события
const checkOnStar = function (user, type, points, game) {
    Finance.findOne({}, function (err, finance) {
        if (finance) {
            if (!user.dayQuest.state && type === finance.currentQuest.targetType) {
                user.dayQuest.value += points;
                if (user.dayQuest.value >= finance.currentQuest.targetValue) {
                    addPoints(user, finance.currentQuest.awardType, finance.currentQuest.awardValue);
                    user.dayQuest.value = finance.currentQuest.targetValue;
                    user.dayQuest.state = true;
                }
            }
            if (game){
                if (!user.dayQuest.state && user.activeAnimal === finance.currentQuest.targetType) {
                    user.dayQuest.value++;
                    if (user.dayQuest.value >= finance.currentQuest.targetValue) {
                        addPoints(user, finance.currentQuest.awardType, +finance.currentQuest.awardValue);
                        user.dayQuest.value = finance.currentQuest.targetValue;
                        user.dayQuest.state = true;
                    }
                }
            }
        }

        let lastStar = user.star;
        if (type === 'Re') {
            user.dayQuest.dayRe += points;
            if (user.Re >= 1000 && user.lotteryState === 'none') {
                addEvent(user.id, 'superLotteryInvite');
            }
            if (user.Re >= 750) {
                if (user.star < 3) {
                    user.star = 3;
                    if (user.maxStar < user.star && user.referral) {
                        user.maxStar = user.star;
                        // addPointsToReferral(user, 'Re', 100, function (err){
                        //     if(err){}
                        // });
                    }
                }
                user.star = 3;
            } else if (user.Re >= 500 && user.Re < 750) {
                if (user.star < 2) {
                    user.star = 2;
                    if (user.maxStar < user.star && user.referral) {
                        user.maxStar = user.star;
                        // addPointsToReferral(user, 'Re', 100, function (err){
                        //     if(err){}
                        // });
                    }
                } else if (user.Re < 700) {
                    user.star = 2;
                }
            } else if (user.Re >= 250 && user.Re < 500) {
                if (user.star < 1) {
                    user.star = 1;
                    if (user.maxStar < user.star && user.referral) {
                        user.maxStar = user.star;
                        addPointsToReferral(user, 'Re', 10, 0, function (err) {
                            if (err) {
                            }
                        });
                    }
                } else if (user.Re < 450) {
                    user.star = 1;
                }
            } else if (user.Re < 250) {
                if (user.Re < 200) {
                    user.star = 0;
                }
            }
        } else if (type === 'Pr') {
            user.dayQuest.dayPr += points;
            if (user.Pr >= 1000 && user.lotteryState === 'none') {
                addEvent(user.id, 'lotteryInvite');
            }
        } else if (type === 'money') {
            user.endMoneyDate = new Date(Date.now() + 30 * 24 * 3600 * 1000);
            addTimer(user.id, 'money', 30 * 24 * 3600 * 1000);
        }
        if (lastStar !== user.star) {
            let i;
            if (user.star > lastStar) {
                for (i = 0; i < user.star - lastStar; i++) {
                    addEvent(user.id, 'gotStar' + (lastStar + i + 1));
                }
            } else {
                for (i = 0; i < lastStar - user.star; i++) {
                    addEvent(user.id, 'lostStar' + (lastStar - i));
                }
            }
        }

        if(game){
            if (user.animalNumber > 0){
                user.animalNumber -= 1;
                if (user.animalNumber === 0) {
                    if (user.activeAnimal !== 'bio' && user.activeAnimal !== 'hamster') {
                        changeAnimal(user, 'hamster');
                    }
                }
            }
        }

        if (user.Al >= roulette.squirrelCost) {
            changeAnimal(user, 'squirrel');
        }

        if (game) {
            if (user.activeAnimal === 'hamster') {
                user.hamsterNumber++;
                if(user.hamsterNumber > 2 && user.Cf > 0){
                    let targetRandom = roulette.PesecChanceSimple;
                    if (user.squirrelNumber >= 3) {
                        user.squirrelNumber = 0;
                        targetRandom = roulette.PesecChanceAfterSquirrel;
                    } else if (user.nextGame.needMinus) {
                        targetRandom = roulette.PesecChanceForLottery;
                    }
                    if (Math.random() < targetRandom) {
                        changeAnimal(user, 'pesec');
                    }
                }
            }

            if (user.lotteryState != 'lottery') {
                Lottery.findOne({type: 'lottery'}, function (err, lottery) {
                    if (lottery) {
                        let lotteryBeforeDay = (lottery.date.getTime() - Date.now()) / (24 * 3600 * 1000);
                        if ((lotteryBeforeDay > 20 && user.dayQuest.dayPr > 50) || ((lotteryBeforeDay > 4 && lotteryBeforeDay <= 20) && user.dayQuest.dayPr > 25)) {
                            let chance = 0;
                            if (lotteryBeforeDay > 20 && user.dayQuest.dayPr > 50){
                                chance = user.dayQuest.dayPr - 50;
                            } else if((lotteryBeforeDay > 4 && lotteryBeforeDay <= 20) && user.dayQuest.dayPr > 25) {
                                chance = user.dayQuest.dayPr - 25;
                            }
                            User.update({_id: user.id}, {
                                'nextGame.type': 'Pr',
                                'nextGame.needMinus': true,
                                'nextGame.chance': chance
                            }, function (err) {
                                if (err) {
                                    log.log('error', 'checkOnStar, lottery: ' + err);
                                }
                            });
                        } else {
                            if (user.nextGame.type == 'Pr') {
                                User.update({_id: user.id}, {
                                    'nextGame.type': '',
                                    'nextGame.needMinus': false
                                }, function (err) {
                                    if (err) {
                                        log.log('error', 'checkOnStar, lottery: ' + err);
                                    }
                                });
                            }
                        }
                    }
                });
            }
            if (user.lotteryState != 'superLottery') {
                Lottery.findOne({type: 'superLottery'}, function (err, superLottery) {
                    if (superLottery) {
                        if (superLottery.state == 'game' && user.Re >= 750) {
                            return;
                        }
                        let lotteryBeforeDay = (superLottery.date.getTime() - Date.now()) / (24 * 3600 * 1000);
                        if (lotteryBeforeDay < 10) {
                            if(user.Re > 850 && user.dayQuest.dayRe > 5){
                                User.update({_id: user.id}, {
                                    'nextGame.type': 'Re',
                                    'nextGame.needMinus': true,
                                    'nextGame.chance': user.dayQuest.dayRe
                                }, function (err) {
                                    if (err) {
                                        log.log('error', 'checkOnStar, superLottery: ' + err);
                                    }
                                });
                            }
                        } else {
                            if ((user.Re < 500 && user.dayQuest.dayRe > 50) || ((user.Re >= 500 && user.Re < 750) && user.dayQuest.dayRe > 25) || ((user.Re >= 750 && user.Re < 850) && user.dayQuest.dayRe > 15)) {
                                let chance = (user.Re < 500 && user.dayQuest.dayRe > 50) ? (user.dayQuest.dayRe - 50) : ((user.Re >= 500 && user.Re < 750) && user.dayQuest.dayRe > 25) ? (user.dayQuest.dayRe - 25) : ((user.Re >= 750 && user.Re < 850) && user.dayQuest.dayRe > 15) ? (user.dayQuest.dayRe - 15) : 0;
                                User.update({_id: user.id}, {
                                    'nextGame.type': 'Re',
                                    'nextGame.needMinus': true,
                                    'nextGame.chance': chance
                                }, function (err) {
                                    if (err) {
                                        log.log('error', 'checkOnStar, superLottery: ' + err);
                                    }
                                });
                            } else if ((user.Re < 500 && user.dayQuest.dayRe < -15) || ((user.Re >= 500 && user.Re < 750) && user.dayQuest.dayRe < -25) || ((user.Re >= 750 && user.Re < 850) && user.dayQuest.dayRe < -50)) {
                                User.update({_id: user.id}, {
                                    'nextGame.type': 'Re',
                                    'nextGame.needMinus': false
                                }, function (err) {
                                    if (err) {
                                        log.log('error', 'checkOnStar, superLottery: ' + err);
                                    }
                                });
                            } else {
                                if (user.nextGame.type == 'Re') {
                                    User.update({_id: user.id}, {
                                        'nextGame.type': '',
                                        'nextGame.needMinus': false
                                    }, function (err) {
                                        if (err) {
                                            log.log('error', 'checkOnStar, superLottery: ' + err);
                                        }
                                    });
                                }
                            }
                        }
                    }
                });
            }
        }

        user.save(function (err) {
            if (err) {
                log.log('error', 'checkOnStar: ' + err);
            }
        });
    });
};
//Получение вопроса по города, в случае отстутсвия выдается любой другой
const oldGetRandomQuestion = function (user, questionType, query, callback) {
    Advertiser.count(query).exec(function (err, count) {
        if (err) {
            log.log('error', 'oldGetRandomQuestion, count: ' + err);
            return callback('Внутренняя ошибка сервера');
        }
        let random = Math.floor(Math.random() * count);

        Advertiser.findOne(query).skip(random).exec(function (err, advertiser) {
            if (err) {
                log.log('error', 'oldGetRandomQuestion: ' + err);
                return callback('Внутренняя ошибка сервера');
            }
            if (!advertiser) {
                return callback('Вопрос не найден!');
            }

            let questions = [];
            let questionsNumber = 1;
            advertiser.dopQuestions.forEach(function (q, i) {
                if (q.questionState === 'ok') {
                    questions.push(i);
                    questionsNumber++;
                }
            });

            let questionNumber = Math.floor(Math.random() * questionsNumber);
            let question = new Answer({
                userId: user.id,
                advertiserId: advertiser.id,
                answerText: questionNumber == 0 ? advertiser.answerText: advertiser.dopQuestions[questions[questionNumber - 1]].answerText,
                questionNumber: questionNumber===0?0:(questions[questionNumber - 1]+1),
                questionType: questionType,
                createdAt: Date.now()
            });
            question.save(function (err) {
                if (err) {
                    log.log('error', 'oldGetRandomQuestion: ' + err);
                    return callback('Внутренняя ошибка сервера');
                }
                if (questionNumber === 0) {
                    return callback(null, [question.id, advertiser.questionText, advertiser.answerText, advertiser.answerSite, questionType === 'question5'?user.questionsState.question5:user.questionsState.question15]);
                } else {
                    return callback(null, [question.id, advertiser.dopQuestions[questions[questionNumber - 1]].questionText, advertiser.dopQuestions[questions[questionNumber - 1]].answerText, advertiser.dopQuestions[questions[questionNumber - 1]].answerSite, questionType === 'question5'?user.questionsState.question5:user.questionsState.question15]);
                }
            });
        });
    });
};
const getRandomQuestion = function (user, number, questionType, callback) {
    if(!user.questionsState.advertisers[number]){
        let query = {_type: 'advertiser', activePacket: true, country: user.country, questionState: 'ok'};
        if(Math.random() > 0.5){
            query.city = user.city;
        } else {
            query.VipByCountryState = true;
        }
        oldGetRandomQuestion(user, questionType, query, callback);
    } else {
        Advertiser.findById(user.questionsState.advertisers[number].userId, function (err, advertiser) {
            if (err) {
                log.log('error', 'getRandomQuestion: ' + err);
                return callback('Внутренняя ошибка сервера');
            }
            if (!advertiser) {
                let query = {_type: 'advertiser', activePacket: true, country: user.country, questionState: 'ok'};
                if(Math.random() > 0.5){
                    query.city = user.city;
                } else {
                    query.VipByCountryState = true;
                }
                oldGetRandomQuestion(user, questionType, query, callback);
                return;
            }

            let questions = [];
            let questionsNumber = 1;
            advertiser.dopQuestions.forEach(function (q, i) {
                if (q.questionState === 'ok') {
                    questions.push(i);
                    questionsNumber++;
                }
            });

            let questionNumber = user.questionsState.advertisers[number].questionNumber%questionsNumber;
            let question = new Answer({
                userId: user.id,
                advertiserId: advertiser.id,
                answerText: questionNumber == 0 ? advertiser.answerText : advertiser.dopQuestions[questions[questionNumber - 1]].answerText,
                questionNumber: questionNumber === 0 ? 0 : (questions[questionNumber - 1] + 1),
                questionType: questionType,
                createdAt: Date.now()
            });

            question.save(function (err) {
                if (err) {
                    log.log('error', 'getRandomQuestion: ' + err);
                    return callback('Внутренняя ошибка сервера');
                }
                if (questionNumber === 0) {
                    return callback(null, [question.id, advertiser.questionText, advertiser.answerText, advertiser.answerSite, questionType === 'question5' ? user.questionsState.question5 : user.questionsState.question15]);
                } else {
                    return callback(null, [question.id, advertiser.dopQuestions[questions[questionNumber - 1]].questionText, advertiser.dopQuestions[questions[questionNumber - 1]].answerText, advertiser.dopQuestions[questions[questionNumber - 1]].answerSite, questionType === 'question5' ? user.questionsState.question5 : user.questionsState.question15]);
                }
            });
        });
    }
};
const createAdvertisersList = function (user, questionType, callback) {
    let query = {_type: 'advertiser', activePacket: true, country: user.country, city: user.city, questionState: 'ok'};
    let needLength = questionType === 'question5'? 3: 8;
    user.questionsState.advertisers = [];
    Advertiser.find(query).limit(needLength).select('_id').exec(function (err, advertisers) {
        if(err){
            log.log('error', 'createAdvertisersList: ' + err);
        }
        let ids = [];
        advertisers.forEach(function (advertiser) {
            ids.push(advertiser._id);
            user.questionsState.advertisers.push({userId: advertiser._id, questionNumber: 0});
        });
        let query1 = {_type: 'advertiser', activePacket: true, country: user.country, VipByCountryState: true, questionState: 'ok'};
        query1._id = {$nin: ids};
        let needLength1 = questionType === 'question5'? 2: 7;
        Advertiser.find(query1).limit(needLength1).select('_id').exec(function (err, advertisers) {
            if(err){
                log.log('error', 'createAdvertisersList: ' + err);
            }

            let i = 1;
            advertisers.forEach(function (advertiser) {
                user.questionsState.advertisers.splice(i - 1, 1, {userId: advertiser._id, questionNumber: 0});
                i += 2;
            });

            user.save(function (err) {
                if (err){
                    log.log('error', 'createAdvertisersList: ' + err);
                }
                return callback();
            });
        });
    });
};
//Получение рекламы по городу или стране, иначе супер-аккаунта
const getRandomAd = function (user, query, callback) {
    Advertiser.count(query).exec(function (err, count) {
        if (err) {
            log.log('error', 'getRandomAd: ' + err);
            return callback('Внутренняя ошибка сервера');
        }
        if (count === 0) {
            return callback('Рекламный блок не найден');
        }
        let random = Math.floor(Math.random() * count);

        Advertiser.findOne(query).skip(random).exec(function (err, advertiser) {
            if (err) {
                log.log('error', 'getRandomAd: ' + err);
                return callback('Внутренняя ошибка сервера');
            }
            if (!advertiser) {
                return callback('Рекламный блок не найден');
            }
            addToAwards(user, advertiser, function (err, result) {
                callback(err, result);
            });
        });
    });
};
//Игра в режиме розыгрыша, проверка на отсутсвие совпадений на первом месте
const playLotteryGame = function (user, lottery, callback) {
    let type = roulette.top.dop[Math.floor(Math.random() * roulette.top.dop.length)];
    let value = roulette.hamster.values[Math.floor(Math.random() * roulette.hamster.values.length)];
    let newValue = 0;
    if (typeof type === 'string' && type.search(/x/i) !== -1) {
        newValue = +value * type.replace('x', '');
    } else {
        newValue = +value + +type;
    }

    let tempArray = user.lotteryPoints;
    user.lotteryPoints = [];
    for (let i = 0; i < user.lotteryNumber; i++) {
        user.lotteryPoints.push(tempArray[i]);
    }
    user.lotteryPoints.push(newValue);
    let userLotteryNumber = user.lotteryNumber + 1;
    for (let i = userLotteryNumber; i < 3; i++) {
        user.lotteryPoints.push(0);
    }

    if (userLotteryNumber === 3) {
        let points = 0;
        user.lotteryPoints.forEach(function (point) {
            points += point;
        });
        if (lottery.type === 'lottery') {
            points = points * user.Re;
        }
        LotteryGame.checkOnWin(lottery.type, points, function (success) {
            if(success){
                let lotteryGame = new LotteryGame({
                    lotteryType: lottery.type,
                    userId: user.id,
                    accountID: user.accountID,
                    points: Math.floor(points)
                });
                lotteryGame.save(function (err) {
                    if (err) {
                        return log.log('error', 'playLotteryGame: ' + err);
                    }
                });
                addEvent(user.id, 'warningRe');

                user.Re = 0;
                user.star = 0;
                user.save(function (err) {
                    if (err) {
                        log.log('error', 'playLotteryGame: ' + err);
                        return callback('Внутренняя ошибка сервера');
                    }
                    callback(null, [type, Math.abs(value)]);
                });
            } else {
                playLotteryGame(user, lottery, callback);
            }
        });
    } else {
        user.save(function (err) {
            if (err) {
                log.log('error', 'playLotteryGame: ' + err);
                return callback('Внутренняя ошибка сервера');
            }
            callback(null, [type, Math.abs(value)]);
        });
    }
};
//Обычная игра, ограничение на набор Pr или Re
const playSimpleGame = function (user, callback) {
    User.findOneAndUpdate({_id: user.id, Cf: {"$gt": 0}}, {$inc: {Cf: -1}}, function (err, user) {
        if (err) {
            log.log('error', 'playSimpleGame: ' + err);
            return callback('Внутренняя ошибка сервера');
        }
        if (!user) {
            return callback('Недостаточно Cf');
        }

        if (user.Cf === 1) {
            addEvent(user.id, 'zeroCf');
        }

        let type;
        let value;
        if (Math.random() < roulette.GtChance) {
            type = 'Gt';
        } else {
            if (user.lotteryState != 'lottery' && user.nextGame.type == 'Pr') {
                if (user.nextGame.needMinus) {
                    if (user.activeAnimal !== 'hamster') {
                        type = '-Pr';
                        let chance = user.nextGame.chance + 10 > 35 ? 35: user.nextGame.chance + 10;
                        value = Math.floor((Math.random() * chance/2) + chance/2);
                    }
                } else if ((user.activeAnimal == 'hamster' || user.activeAnimal == 'pesec') && Math.random() < roulette.LotteryUpControl) {
                    type = 'Pr';
                    value = Math.floor(Math.random() * 15);
                }
            } else if (user.lotteryState != 'superLottery' && user.nextGame.type == 'Re') {
                if (user.nextGame.needMinus) {
                    if (user.activeAnimal !== 'hamster') {
                        type = '-Re';
                        let chance = user.nextGame.chance + 10 > 35 ? 35: user.nextGame.chance + 10;
                        value = Math.floor((Math.random() * chance/2) + chance/2);
                    }
                } else if ((user.activeAnimal == 'hamster' || user.activeAnimal == 'pesec') && Math.random() < roulette.LotteryUpControl) {
                    type = 'Re';
                    value = Math.floor(Math.random() * 15);
                }
            }
            if (!type) {
                type = roulette[user.activeAnimal].types[Math.floor(Math.random() * roulette[user.activeAnimal].types.length)];

                while ((user.lotteryState === 'lottery' && (type === 'Pr' || type === '-Pr')) ||
                (user.lotteryState === 'superLottery' && (type === 'Re' || type === '-Re')) ||
                (user.nextGame.type === "Pr" && ((user.nextGame.needMinus && (type === 'Pr' || type == '-Re')) ||
                (!user.nextGame.needMinus && type === '-Pr'))) ||
                (user.nextGame.type === "Re" && ((user.nextGame.needMinus && type === 'Re') ||
                (!user.nextGame.needMinus && type === '-Re')))) {
                    type = roulette[user.activeAnimal].types[Math.floor(Math.random() * roulette[user.activeAnimal].types.length)];
                }
            }
        }

        if(!value){
            do {
                value = roulette.hamster.values[Math.floor(Math.random() * roulette.hamster.values.length)];
                if (user.activeAnimal === 'squirrel') {
                    if (value <= 5) {
                        break;
                    }
                } else if (user.activeAnimal === 'pesec') {
                    if (value <= 25) {
                        break;
                    }
                } else if (user.activeAnimal === 'bio') {
                    if (value <= 5) {
                        break;
                    }
                } else {
                    break;
                }
            } while (true);
        }

        if (type != 'Gt') {
            if (value >= 0 && type.search(/-/i) !== -1) {
                value *= -1;
            }
            addPoints(user, type.replace('-', ''), +value, true);
        } else {
            user.awards.forEach(function (award) {
                if(award.companyId.toString() === user.lastAwardID){
                    award.currentGt += Math.abs(value);
                    if(award.currentGt >= 100){
                        award.currentGt = 0;
                        Advertiser.findById(award.companyId, function (err, advertiser) {
                            if (err) {
                                log.log('error', 'playSimpleGame: ' + err);
                            }
                            if (advertiser) {
                                addPoints(user, advertiser.VipByGameAward.type, advertiser.VipByGameAward.value, true);
                            } else {
                                checkOnStar(user, 'Gt', 0, true);
                            }
                        });
                        user.save(function (err) {
                            if (err) {
                                log.log('error', 'playSimpleGame: ' + err);
                            }
                        });
                    } else {
                        checkOnStar(user, 'Gt', 0, true);
                    }
                    return award;
                }
            });
        }

        callback(null, [type, Math.abs(value)]);
    });
};
//Добавление очков рефералу
const addPointsToReferral = function (user, type, points, checkNumber, callback) {
    if (points > 0) {
        User.findById(user.referral, function (err, referral) {
            if (err) {
                return log.log('error', 'addPointsToReferral: ' + err);
            }
            if (referral && (referral[type] || referral[type] === 0)) {
                if (type === 'money') {
                    let exist = false;
                    referral.referrers.advertiser.forEach(function (referrer) {
                        if (referrer.companyId && referrer.companyId.toString() === user.id) {
                            exist = true;
                            referrer.cost = referrer.cost + points;
                            referrer.checks.push(checkNumber);
                            return referrer;
                        }
                    });
                    if(!exist) {
                        User.update({_id: referral.id}, {
                            '$push': {
                                'referrers.advertiser': {
                                    companyId: user.id,
                                    cost: points,
                                    checks: [checkNumber]
                                }
                            }
                        }, function (err) {
                            if (err) {
                                return log.log('error', 'addPointsToReferral: ' + err);
                            }
                        });
                    }
                    addPoints(referral, type, points);
                } else {
                    referral.referrers.user.forEach(function (referrer) {
                        if (referrer.toString() === user.id) {
                            if (user.star === 1) {
                                referral.referrers.user.pull(referrer);
                                addPoints(referral, type, points);
                                return referrer;
                            }
                        }
                    });
                }
            }
            referral.save(function (err) {
                if (err) {
                    log.log('error', 'addPointsToReferral: ' + err);
                    return callback('Внутренняя ошибка сервера');
                }
                callback(null);
            });
        });
    }
};
//Дабаление пользователей в кладовку
const addToAwards = function (user, advertiser, callback) {
    let exist = false;
    let advertiserAward;

    user.awards.forEach(function (award) {
        if (award.companyId && award.companyId.toString() === advertiser.id) {
            advertiserAward = award;
            exist = true;
        }
    });
    if (!exist) {
        advertiserAward = {
            companyId: advertiser.id,
            currentGt: 0
        };
        user.awards.push(advertiserAward);
    }

    user.lastAwardID = advertiser.id;
    user.save(function (err) {
        if (err) {
            log.log('error', 'addToAwards: ' + err);
            return callback('Внутренняя ошибка сервера');
        }
        callback(null, [advertiser.id, advertiser.companyName, advertiser.city, advertiser.sphereOfActivity, advertiser.VipText, advertiserAward.currentGt, advertiser.VipByGameAward.value, advertiser.VipByGameAward.type]);
    });
};
//Проверка на блокировку некорректных вопросов и их блокировка раз в сутки
const checkOnBanAnswers = function () {
    Advertiser.find({_type: 'advertiser'}, function (err, advertisers) {
        if (err) {
            return log.log('error', 'checkOnBanAnswers: ' + err);
        }
        advertisers.forEach(function (advertiser) {
            if(advertiser.totalAnswers >= 100 && advertiser.falseAnswers/advertiser.totalAnswers > 0.5){
                advertiser.questionState = 'ban';

                let question = {
                    questionText: advertiser.questionText,
                    answerText: advertiser.answerText,
                    answerSite: advertiser.answerSite
                };

                mailer.sendMail({
                    theme: 'mail_template',
                    sub_theme: 'bananswer',
                    address: advertiser.email,
                    question: question,
                    baseQuestion: true,
                    subject: 'Заблокирован основной вопрос'
                }, function (err) {
                    if (err) {
                        return log.log('error', 'Ошибка при отправке сообщения');
                    }
                });
            }
            advertiser.falseAnswers = 0;
            advertiser.totalAnswers = 0;
            advertiser.dopQuestions.forEach(function (question) {
                if(question.totalAnswers >= 100 && question.falseAnswers/question.totalAnswers > 0.5){
                    question.questionState = 'ban';

                    mailer.sendMail({
                        theme: 'mail_template',
                        sub_theme: 'bananswer',
                        address: advertiser.email,
                        question: question,
                        baseQuestion: false,
                        subject: 'Заблокирован дополнительный вопрос'
                    }, function (err) {
                        if (err) {
                            return log.log('error', 'Ошибка при отправке сообщения');
                        }
                    });
                }
                question.falseAnswers = 0;
                question.totalAnswers = 0;
            });

            advertiser.save(function (err) {
                if (err) {
                    log.log('error', 'checkOnBanAnswers: ' + err);
                }
            });
        });
    });
};
//Создание таймера
const addTimer = function (userId, type, time) {
    Timer.findOne({userId: userId, type: type}, function (err, timer) {
        if (err) {
            return log.log('error', 'addTimer: ' + err);
        }
        if (timer) {
            if(type == 'money' || type == 'bio' || type == 'freebie') {
                timer.date = new Date(Date.now() + time);
            } else {
                timer.date = new Date(timer.date.getTime() + time);
            }
        } else {
            timer = new Timer({
                userId: userId,
                type: type,
                date: Date.now() + time
            });
        }
        switch (timer.type) {
            case 'money':
            case 'prolongation':
            case 'VipByGame':
            case 'VipByCountry':
                timer.notification = false;
                break;
        }
        timer.save(function (err) {
            if (err) {
                log.log('error', 'addTimer: ' + err);
            }
        });
    });
};
//Создание розыгрыша
const addLottery = function (type, time) {
    let lottery = new Lottery({
        type: type,
        date: Date.now() + time
    });
    lottery.save(function (err) {
        if (err) {
            log.log('error', 'addLottery: ' + err);
        }
    });
};
//Проверка событий таймеров
const runTimer = function () {
    setInterval(function () {
        Lottery.find({date: {"$lte": Date.now()}}, function (err, lotteries) {
            if (err) {
                return log.log('error', 'runTimer: ' + err);
            }
            lotteries.forEach(function (lottery) {
                if (lottery.type === 'lottery') { // Обычный розыгрыш
                    if (lottery.state === 'waiting') {
                        lottery.date = new Date(Date.now() + 24 * 3600 * 1000);
                        lottery.state = 'game';

                        User.find({}, function (err, users) {
                            if (users && users.length > 0) {
                                users.forEach(function (user) {
                                    addEvent(user.id, 'lotteryStartWarning');
                                });
                            }
                        });
                        User.update({}, {Pr: 0}, {multi: true}, function (err) {
                            if (err) {
                                return log.log('error', 'runTimer: ' + err);
                            }
                        });
                        User.find({lotteryState: 'lottery'}, function (err, users) {
                            if (err) {
                                return log.log('error', 'runTimer: ' + err);
                            }
                            users.forEach(function (user) {
                                addEvent(user.id, 'lotteryStart');
                            });
                        });
                    } else {
                        LotteryGame.find({lotteryType: 'lottery'}).sort({'points': -1}).limit(1).exec(function (err, players) {
                            if (players.length !== 0 && players[0]) {
                                User.findById(players[0].userId, function (err, user) {
                                    if (err) {
                                        return log.log('error', 'runTimer: ' + err);
                                    }
                                    if (user) {
                                        addEvent(user.id, 'lotteryVictory');
                                        let victory = new Victory({
                                            accountID: user.accountID,
                                            userMail: user.email,
                                            victoryType: 'lottery',
                                            date: Date.now()
                                        });
                                        victory.save(function (err) {
                                            if (err) {
                                                return log.log('error', 'runTimer: ' + err);
                                            }
                                            mailer.sendMail({
                                                theme: 'mail_template',
                                                sub_theme: 'victory',
                                                address: user.email,
                                                accountID: user.accountID,
                                                type: 'lottery',
                                                subject: 'Победа в розыгрыше',
                                            }, function (err) {
                                                if (err) {
                                                    return log.log('error', 'Ошибка при отправке сообщения');
                                                }
                                            });
                                        });
                                    }
                                });
                            }
                            LotteryGame.remove({lotteryType: 'lottery'}, function (err) {
                                if (err) {
                                    return log.log('error', 'runTimer: ' + err);
                                }
                            });

                            User.find({lotteryState: 'lottery', lotteryNumber: {$lt: 3}}, function (err, users) {
                                if(users && users.length > 0){
                                    users.forEach(function (user) {
                                        addEvent(user.id, 'warningRe');
                                    });
                                }
                                User.update({lotteryState: 'lottery', lotteryNumber: {$lt: 3}}, {Re: 0, star: 0}, {multi: true}, function (err) {
                                    if (err) {
                                        return log.log('error', 'runTimer: ' + err);
                                    }
                                });
                                User.update({lotteryState: 'lottery'}, {
                                    lotteryState: 'none',
                                    lotteryNumber: 0, lotteryPoints: [0, 0, 0]
                                }, {multi: true}, function (err) {
                                    if (err) {
                                        return log.log('error', 'runTimer: ' + err);
                                    }
                                });
                            });
                            lottery.date = new Date(Date.now() + 30 * 24 * 3600 * 1000);
                            lottery.state = 'waiting';
                            Finance.findOne({}, function (err, finance) {
                                if (err) {
                                    return log.log('error', 'runTimer: ' + err);
                                }
                                if (finance) {
                                    lottery.text = finance.lottery.text;
                                    lottery.url = finance.lottery.url;
                                    finance.lottery.currentText = lottery.text;
                                    finance.lottery.currentUrl = lottery.url;
                                    finance.save(function (err) {
                                        if(err){
                                            return log.log('error', 'runTimer: ' + err);
                                        }
                                    });
                                    lottery.save(function (err) {
                                        if (err) {
                                            return log.log('error', 'runTimer: ' + err);
                                        }
                                    });
                                }
                            });
                        });
                    }
                } else if (lottery.type === 'superLottery') { // Розыгрыш суперприза
                    if (lottery.state === 'waiting') {

                        lottery.date = new Date(Date.now() + 24 * 3600 * 1000);
                        lottery.state = 'game';

                        User.find({lotteryState: 'superLottery'}, function (err, users) {
                            if (err) {
                                return log.log('error', 'runTimer: ' + err);
                            }
                            users.forEach(function (user) {
                                addEvent(user.id, 'superLotteryStart');
                            });
                        });
                    } else if (lottery.state === 'pause') {
                        lottery.state = 'waiting';
                        lottery.date = new Date(Date.now() + 90 * 24 * 3600 * 1000);

                        User.find({}, function (err, users) {
                            if (err) {
                                return log.log('error', 'runTimer: ' + err);
                            }
                            users.forEach(function (user) {
                                addEvent(user.id, 'superLotteryCountdownStart');
                            });
                        });
                        User.find({Re: 1000}, function (err, users) {
                            if (err) {
                                return log.log('error', 'runTimer: ' + err);
                            }
                            users.forEach(function (user) {
                                addEvent(user.id, 'superLotteryInvite');
                            });
                        });
                    } else {
                        LotteryGame.find({lotteryType: 'superLottery'}).sort({'points': -1}).limit(1).exec(function (err, players) {
                            if (players.length !== 0 && players[0]) {
                                User.findById(players[0].userId, function (err, user) {
                                    if (err) {
                                        return log.log('error', 'runTimer: ' + err);
                                    }
                                    if (user) {
                                        addEvent(user.id, 'superLotteryVictory');
                                        let victory = new Victory({
                                            accountID: user.accountID,
                                            userMail: user.email,
                                            victoryType: 'superLottery',
                                            date: Date.now()
                                        });
                                        victory.save(function (err) {
                                            if (err) {
                                                return log.log('error', 'runTimer: ' + err);
                                            }
                                            mailer.sendMail({
                                                theme: 'mail_template',
                                                sub_theme: 'victory',
                                                address: user.email,
                                                accountID: user.accountID,
                                                type: 'superLottery',
                                                subject: 'Победа в супер-розыгрыше'
                                            }, function (err) {
                                                if (err) {
                                                    log.log('error', 'Ошибка при отправке сообщения');
                                                }
                                            });
                                        });
                                    }
                                });
                            }
                            LotteryGame.remove({lotteryType: 'superLottery'}, function (err) {
                                if (err) {
                                    return log.log('error', 'runTimer: ' + err);
                                }
                            });

                            User.find({lotteryState: 'superLottery', lotteryNumber: {$lt: 3}}, function (err, users) {
                                if (users && users.length > 0) {
                                    users.forEach(function (user) {
                                        addEvent(user.id, 'warningRe');
                                    });
                                }
                                User.update({lotteryState: 'superLottery', lotteryNumber: {$lt: 3}}, {
                                    Re: 0,
                                    star: 0
                                }, {multi: true}, function (err) {
                                    if (err) {
                                        return log.log('error', 'runTimer: ' + err);
                                    }
                                });
                                User.update({lotteryState: 'superLottery'}, {
                                    lotteryState: 'none',
                                    lotteryNumber: 0, lotteryPoints: [0, 0, 0]
                                }, {multi: true}, function (err) {
                                    if (err) {
                                        return log.log('error', 'runTimer: ' + err);
                                    }
                                });
                            });
                            lottery.state = 'pause';
                            lottery.date = new Date(Date.now() + 90 * 24 * 3600 * 1000);
                            lottery.save(function (err) {
                                if (err) {
                                    log.log('error', 'runTimer: ' + err);
                                }
                            });
                        });
                    }
                } else if (lottery.type === 'dayQuest') {
                    lottery.date = new Date(Date.now() + 24 * 3600 * 1000);
                    checkOnBanAnswers();

                    Finance.findOne(function (err, finance) {
                        if(finance){
                            finance.lastQuestNumber++;
                            if(finance.lastQuestNumber >= quests.quests.length) {
                                finance.lastQuestNumber = 0;
                            }
                            finance.currentQuest.targetType = quests.quests[finance.lastQuestNumber].targetType;
                            finance.currentQuest.targetValue = quests.quests[finance.lastQuestNumber].targetValue;
                            finance.currentQuest.awardType = quests.quests[finance.lastQuestNumber].awardType;
                            finance.currentQuest.awardValue = quests.quests[finance.lastQuestNumber].awardValue;
                            finance.currentQuest.date = lottery.date;
                            finance.save(function (err) {
                                if(err){
                                    return log.log('error', 'runTimer: ' + err);
                                }
                            });
                        }
                    });
                    User.update({}, {'dayQuest.value': 0, 'dayQuest.dayAnswers': 0, 'dayQuest.dayPr': 0, 'dayQuest.dayRe': 0, 'dayQuest.state': false}, {multi: true}, function (err) {
                        if (err) {
                            return log.log('error', 'runTimer: ' + err);
                        }
                    });
                }
                lottery.save(function (err) {
                    if (err) {
                        log.log('error', 'runTimer: ' + err);
                    }
                });
            });
        });
        Timer.find({notification: true, date: {"$lte": Date.now()}}).populate('userId').exec(function (err, timers) {
            if (err) {
                return log.log('error', 'runTimer: ' + err);
            }
            timers.forEach(function (timer) {
                let themes = [];
                let values = [];

                let user = timer.userId;
                if (!user) {
                    timer.remove(function (err) {
                        if (err) {
                            log.log('error', 'runTimer: ' + err);
                        }
                    });
                    return log.log('info', 'User not found');
                }

                switch (timer.type) {
                    case 'freebie':
                        user.likeList = [];
                        user.freebie = false;
                        break;
                    case 'prolongation':
                        themes.push('ended_packet');
                        values.push(user.packetType);
                        if(user.packetType === 'Тестовый'){
                            User.update({_id: user.referral}, {$pull: {'referrers.advertiser': {companyId: user.id}}}, {safe: true}, function (err) {
                                if(err){
                                    return log.log('error', 'runTimer: ' + err);
                                }
                            });
                        }
                        user.activePacket = false;
                        break;
                    case 'VipByGame':
                        themes.push('ended_packet');
                        values.push('VIP в игре');
                        user.VipByGameState = false;
                        break;
                    case 'VipByCountry':
                        themes.push('ended_packet');
                        values.push('VIP по стране');
                        user.VipType = 'VipByGame';
                        user.VipByCountryState = false;
                        break;
                    case 'bio':
                        changeAnimal(user, 'hamster');
                        break;
                    case 'money':
                        let array = ['Re', 'Pr', 'Lu', 'Al', 'Cf'];
                        while (user.money >= 500) {
                            addPoints(user, array[Math.floor(Math.random() * array.length)], 5);
                            user.money -= 500;
                        }
                        User.update({_id: user.id}, {money: 0, 'referrers.advertiser': []}, function (err){
                            if (err) {
                                log.log('error', 'runTimer: ' + err);
                            }
                        });
                        break;
                }
                themes.forEach(function (theme, i) {
                    mailer.sendMail({
                        theme: 'mail_template',
                        sub_theme: theme,
                        address: user.email,
                        value: values[i],
                        subject: 'Срок истек',
                    }, function (err) {
                        if (err) {
                            return log.log('error', 'Ошибка при отправке сообщения');
                        }
                    });
                });

                timer.remove(function (err) {
                    if (err) {
                        log.log('error', 'runTimer: ' + err);
                    }
                    user.save(function (err) {
                        if (err) {
                            log.log('error', 'runTimer: ' + err);
                        }
                    });
                });
            });
        });
        Timer.find({notification: false, date: {"$lte": new Date(Date.now() + 7*24*3600*1000)}}).populate('userId').exec(function (err, timers) {
            if (err) {
                return log.log('error', 'runTimer: ' + err);
            }
            timers.forEach(function (timer) {
                let user = timer.userId;
                if (!user) {
                    log.log('info', 'User not found');
                } else {
                    let themes = [];
                    let values = [];
                    switch (timer.type) {
                        case 'prolongation':
                            themes.push('end_packet');
                            values.push(user.packetType);
                            break;
                        case 'VipByGame':
                            themes.push('end_packet');
                            values.push('VIP в игре');
                            break;
                        case 'VipByCountry':
                            themes.push('end_packet');
                            values.push('VIP по стране');
                            break;
                        case 'money':
                            if(user.money > 0){
                                themes.push('end_money');
                                values.push(user.money + ' Р.');
                            }
                            break;
                    }
                    themes.forEach(function (theme, i) {
                        mailer.sendMail({
                            theme: 'mail_template',
                            sub_theme: theme,
                            address: user.email,
                            value: values[i],
                            subject: 'Истекает срок',
                        }, function (err) {
                            if (err) {
                                return log.log('error', 'Ошибка при отправке сообщения');
                            }
                        });
                    });
                }
                timer.notification = true;
                timer.save(function (err) {
                    if (err) {
                        log.log('error', 'runTimer: ' + err);
                    }
                });
            });
        });
    }, 1000);
    setInterval(function () {
        Email.find({}).sort({'date': 1}).limit(10).exec(function (err, emails) {
            emails.forEach(function (email) {
		email.remove(function(err){});
                mailer.sendMail({
                    theme: 'resend',
                    address: email.address,
                    subject: email.subject,
                    html: email.html,
                    emailID: email.id
                }, function (err) {});
	    });
        });
    }, 1000);
};

module.exports = {
    startup: function (dbToUse, Mailer) {
        mailer = Mailer;
        mailer.setEmail(Email);
        mongoose.connect(dbToUse, options, function (err) {
            if(err){
                throw err;
            }
        });
        Finance.findOne({}, function (err, finance) {
            if (err) {
                return log.log('error', 'startup: ' + err);
            }
            //Первый запуск, создание розыгрышей
            if (!finance) {
                finance = new Finance();
                addLottery('dayQuest', 0);
                addLottery('lottery', 30*24*3600*1000);
                addLottery('superLottery', 90*24*3600*1000);
                finance.save(function (err) {
                    if (err) {
                        return log.log('error', 'startup: ' + err);
                    }
                });
                Lottery.findOneAndUpdate({type: 'lottery'}, {text: finance.lottery.currentText, url: finance.lottery.currentUrl}, function (err){
                    if(err){
                        log.log('error', 'startup: ' + err);
                    }
                });
                Lottery.findOneAndUpdate({type: 'superLottery'}, {state: "pause", text: roulette.superLottery.text, url: roulette.superLottery.url}, function (err){
                    if(err){
                        log.log('error', 'startup: ' + err);
                    }
                });
            }
        });
        mongoose.connection.on('open', function () {
            log.log('info', 'Connected to database!');
            runTimer();
        });
    },

    //Simple user section
    authenticate: function (accountID, password, callback) {
        User.authenticate(accountID, password, function (err, user) {
            if (err) {
                log.log('error', 'authenticate: ' + err);
                return callback('Внутренняя ошибка сервера');
            }
            if (!user) {
                return callback('Пользователь не найден');
            }
            crypto.randomBytes(20, function (err, buffer) {
                let tokenId = buffer.toString('hex');

                Token.remove({userId: user.id}, function (err) {
                    if (err) {
                        return log.log('error', 'authenticate: ' + err);
                    }
                });
                let token = new Token({
                    userId: user.id,
                    tokenId: tokenId,
                    userType: user._type ? user._type : 'user'
                });
                token.save(function (err) {
                    if (err) {
                        log.log('error', 'authenticate: ' + err);
                        return callback('Внутренняя ошибка сервера');
                    }
                });

                Victory.findOne({accountID: user.accountID, verifyUser: false}, function (err, victory) {
                    if (err) {
                        return log.log('error', 'authenticate: ' + err);
                    }
                    if (victory) {
                        addEvent(user.id, victory.victoryType + 'Victory');
                    }
                });

                if (user.lotteryState === 'none') {
                    if (user.Pr >= 1000) {
                        addEvent(user.id, 'lotteryInvite');
                    }

                    if (user.Re >= 1000) {
                        Lottery.findOne({type: 'superLottery'}, function (err, lottery) {
                            if (err) {
                                return log.log('error', 'authenticate: ' + err);
                            }

                            if (!lottery) {
                                return log.log('error', 'Внутренняя ошибка');
                            }
                            if (lottery.state !== 'pause') {
                                addEvent(user.id, 'superLotteryInvite');
                            }
                        });
                    }
                } else if (user.lotteryNumber < 3) {
                    Lottery.findOne({type: user.lotteryState}, function (err, lottery) {
                        if (err) {
                            return log.log('error', 'authenticate: ' + err);
                        }
                        if (lottery && lottery.state === 'game') {
                            addEvent(user.id, user.lotteryState + 'Start');
                        }
                    });
                }

                return callback(null, [tokenId, true, user._type ? user._type : 'user']);
            });
        })
    },
    createInvite: function (user, args, callback) {
        let self = this;
        User.findOne({email: args[2]}, function (err, oldUser) {
            if (oldUser) {
                return callback('Этот email уже занят!');
            }
            if ((((user && user._type === 'administrator') || args[4] === 'verySecretKeyForAdminRegister') && args[1] === 'administrator') || (args[1] === 'user' || args[1] === 'advertiser')) {
                crypto.randomBytes(4, function (err, buffer) {
                    let invite = buffer.toString('hex');
                    let newAccountID = 'x' + Math.floor(Math.random() * 10000) + 'w' + Math.floor(Math.random() * 100000);
                    if (args[1] === 'administrator') {
                        newAccountID = 'a' + Math.floor(Math.random() * 10000) + 'd' + Math.floor(Math.random() * 100000);
                    } else if (args[1] === 'advertiser') {
                        newAccountID = 'a' + Math.floor(Math.random() * 10000) + 'v' + Math.floor(Math.random() * 100000);
                    }
                    User.findOne({accountID: newAccountID}, function (err, checkUser) {
                        if (checkUser) {
                            return self.createInvite(user, args, callback);
                        }
                        let inviteModel = new Invite({
                            invite: invite,
                            accountID: newAccountID,
                            type: args[1],
                            email: args[2],
                            createdAt: Date.now()
                        });
                        let accountID;
                        if (user) {
                            inviteModel.userId = user.id;
                            accountID = user._type === 'administrator' ? 'admin' : user.accountID;
                        }
                        if (args[4] === 'verySecretKeyForAdminGift') {
                            Administrator.findOne({_type: 'administrator'}, function (err, admin) {
                                if (err) {
                                    log.log('error', 'createInvite: ' + err);
                                    return callback('Внутренняя ошибка сервера');
                                }
                                inviteModel.userId = admin.id;

                                inviteModel.save(function (err) {
                                    if (err) {
                                        log.log('error', 'createInvite: ' + err);
                                        return callback('Внутренняя ошибка сервера');
                                    }
                                    Finance.findOne({}, function (err, finance) {
                                        if (err) {
                                            log.log('error', 'createInvite: ' + err);
                                        }
                                        if (finance) {
                                            return callback(null, {
                                                invite: invite,
                                                accountID: 'admin',
                                                Pr: 100
                                            });
                                        }
                                        log.log('info', 'No finance!');
                                    });
                                });
                            });
                        } else {
                            inviteModel.save(function (err) {
                                if (err) {
                                    log.log('error', 'createInvite: ' + err);
                                    return callback('Внутренняя ошибка сервера');
                                }
                                Finance.findOne({}, function (err, finance) {
                                    if (err) {
                                        log.log('error', 'createInvite: ' + err);
                                    }
                                    if (finance) {
                                        return callback(null, {
                                            invite: invite,
                                            accountID: accountID,
                                            Pr: 100
                                        });
                                    }
                                    log.log('info', 'No finance!');
                                });
                            });
                        }
                    });
                });
            } else {
                callback('Неверный тип пользователя');
            }
        });
    },
    checkInvite: function (args, callback) {
        Invite.findOne({invite: args.invite}, function (err, invite) {
            if (!invite) {
                return callback('Инвайт не найден');
            }
            return callback(null, {accountID: invite.accountID, type: invite.type, email: invite.email});
        });
    },
    sendResetCode: function (email, callback) {
        User.findOne({email: email}, function (err, user) {
            if (err) {
                log.log('error', 'sendResetCode: ' + err);
                return callback('Внутренняя ошибка сервера');
            }
            if (!user) {
                return callback('Пользователь не найден');
            }
            crypto.randomBytes(6, function (err, buffer) {
                let resetCode = buffer.toString('hex');
                user.resetCode = resetCode;
                user.save(function (err) {
                    if(err){
                        log.log('error', 'sendResetCode: ' + err);
                        return callback('Внутренняя ошибка сервера');
                    }
                    mailer.sendMail({
                        theme: 'mail_template',
                        sub_theme: 'passreset',
                        address: user.email,
                        resetCode: resetCode,
                        subject: 'Сброс пароля',
                    }, function (err) {
                        if (err) {
                            return log.log('error', 'Ошибка при отправке сообщения');
                        }
                    });
                    callback(null, [true]);
                });
            });
        });
    },
    resetPassword: function (resetCode, callback) {
        crypto.randomBytes(4, function (err, buffer) {
            let password = buffer.toString('hex');
            User.findOneAndUpdate({resetCode: resetCode}, {password: password, resetCode: ""}, function (err, user) {
                if (err) {
                    log.log('error', 'resetPassword: ' + err);
                    return callback('Внутренняя ошибка сервера');
                }
                if (!user) {
                    return callback('Пользователь не найден');
                }

                callback(null, [password]);
            });
        });
    },
    saveUser: function (args, callback) {
        let type = 'user';
        Invite.findOne({invite: args.invite}, function (err, invite) {
            if (err) {
                log.log('error', 'saveUser: ' + err);
                return callback('Внутренняя ошибка сервера');
            }
            if (!invite) {
                return callback('Инвайт не найден');
            }
            type = invite.type;
            args.type = type;
            args.referral = invite.userId;
            args.accountID = invite.accountID;
            args.email = invite.email;

            createUser(args, function (err, newUser) {
                if (err) {
                    log.log('error', 'saveUser: ' + err);
                    return callback('Внутренняя ошибка сервера');
                }
                if (newUser.referral) {
                    let queryUpdate = {'$push': {'referrers.user': newUser.id}};
                    if(type === 'advertiser') {
                        queryUpdate = {'$push': {'referrers.advertiser': {companyId: newUser.id, cost: 0}}};
                    }
                    User.update({_id: newUser.referral}, queryUpdate, function (err) {
                        if (err) {
                            return log.log('error', 'saveUser: ' + err);
                        }
                    });
                }
                invite.remove(function (err) {
                    if(err){
                        log.log('error', 'saveUser: ' + err);
                        return callback('Внутренняя ошибка сервера');
                    }
                    callback(null, newUser);
                });
            });
        });
    },

    //AfterParty section
    uploadImage: function (user, callback) {
        if (user.star < 1) {
            return callback('Звёзд должно быть больше одной');
        }
        let image = new Images({
            accountID: user.accountID,
            createdAt: Date.now()
        });
        image.save(function (err) {
            if (err) {
                log.log('error', 'uploadImage: ' + err);
                return callback('Внутренняя ошибка сервера');
            }
            callback(null, [true, image.id]);
        });
    },
    removeImage: function (imageId, callback) {
        Images.findByIdAndRemove(imageId, function (err, image) {
            if (err) {
                log.log('error', 'removeImage: ' + err);
                return callback('Внутренняя ошибка сервера');
            }
            if(!image){
                return callback('Не найдено');
            }
            User.findOneAndUpdate({accountID: image.accountID}, {imagePath: image.accountID}, function (err) {
                if(err){
                    log.log('error', 'removeImage: ' + err);
                }
            });
            return callback(null, [image.accountID]);
        });
    },
    joinParty: function (user, callback) {
        if (user.star < 1) {
            return callback('Ошибка запроса.\nВы не можете размещаться в галерее не получив\nпервой звезды.');
        } else if(user.lotteryState !== "none"){
            return callback('До конца розыгрыша ' + (user.lotteryState === 'lottery') ? 'ПРИЗА': 'СУПЕРПРИЗА' +' автопати недоступна');
        } else if(user.freebie) {
            return callback('Ошибка запроса.\nВы уже в галерее.');
        } else if(user.motivationTextDate.getTime() - 6*24*3600*1000 > Date.now()){
            return callback("Ошибка запроса.\nВы недавно размещались в галерее.\nПодождите.");
        }

        User.count({city: user.city, freebie: true}, function (err, count) {
            if (err){
                log.log('error', 'joinParty: ' + err);
                return callback('Внутренняя ошибка сервера');
            }
            if(count > 150){
                return callback(null, [false]);
            }
            user.freebie = true;
            user.motivationTextDate = new Date(Date.now() + 7*24*3600*1000);
            addTimer(user.id, 'freebie', 7*24*3600*1000);
            user.save(function (err) {
                if (err){
                    log.log('error', 'joinParty: ' + err);
                    return callback('Внутренняя ошибка сервера');
                }
                callback(null, [true]);
            });
        });
    },
    leaveParty: function (user, callback) {
        if(!user.freebie){
            return callback('Вы не соостоите в галерее');
        }

        Timer.remove({type: 'freebie', userId: user.id}, function (err) {
            if (err){
                log.log('error', 'leaveParty: ' + err);
            }
        });
        user.likeList = [];
        user.freebie = false;
        user.save(function (err) {
            if (err) {
                log.log('error', 'leaveParty: ' + err);
                return callback('Внутренняя ошибка сервера');
            }
            callback(null, [true]);
        });
    },
    getFreebies: function (user, callback) {
        User.find({city: user.city, freebie: true}, function (err, users) {
            if (err) {
                log.log('error', 'getFreebies: ' + err);
                return callback('Внутренняя ошибка сервера');
            }

            if (users.length === 0) {
                return callback(null, [false]);
            }

            let array = [];
            users.sort(function (a, b) {
                if (a.Pr > b.Pr) {
                    return 1;
                }
                if (a.Pr < b.Pr) {
                    return -1;
                }
                return 0;
            });

            users.forEach(function (u) {
                array.push(u.accountID, u.Pr);
            });
            callback(null, [array]);
        });
    },
    getFreebieInfo: function (user, args, callback) {
        User.findOne({accountID: args.id}, function (err, freebie) {
            if (err) {
                log.log('error', 'getFreebieInfo: ' + err);
                return callback('Внутренняя ошибка сервера');
            }
            if (!freebie) {
                return callback('Пользователь не найден');
            }
            freebie.numberOfViews++;
            freebie.save(function (err) {
                if (err) {
                    log.log('error', 'getFreebieInfo: ' + err);
                    return callback('Внутренняя ошибка сервера');
                }
                let liked = false;
                let vote = false;
                freebie.likeList.forEach(function (like) {
                    if (like.accountID === user.accountID) {
                        liked = true;
                        vote = like.like;
                    }
                });
                callback(null, [freebie.imagePath, freebie.motivationText, freebie.numberOfLikes, freebie.numberOfDislikes, liked, vote]);
            });
        });
    },
    likeDislikeFreebie: function (user, args, callback) {
        if((user.star < 2 && args.like) || (user.star < 3 && !args.like)){
            return callback('Недостаточно звёзд для действия');
        } else if(user.lotteryState !== "none"){
            return callback('До конца розыгрыша ' + (user.lotteryState === 'lottery') ? 'ПРИЗА': 'СУПЕРПРИЗА' +' автопати недоступна');
        }
        User.findOneAndUpdate({accountID: args.id, 'likeList.accountID': {$ne: user.accountID}}, {"$push": {likeList: {accountID: user.accountID, like: args.like, phanks: false}}}, {new: true}, function (err, freebie) {
            if (err) {
                log.log('error', 'likeDislikeFreebie: ' + err);
                return callback('Внутренняя ошибка сервера');
            }
            if (!freebie) {
                return callback('Уже оценено');
            }

            Finance.findOne({}, function (err, finance) {
                if (err) {
                    log.log('error', 'likeDislikeFreebie: ' + err);
                    return callback('Внутренняя ошибка сервера');
                }
                if (!finance) {
                    return callback('Внутренняя ошибка сервера');
                }
                let eventType;
                if (args.like) {
                    freebie.numberOfLikes++;
                    addPoints(freebie, 'Pr', finance.afterParty.give.cost);
                    addPoints(user, 'Pr', -finance.afterParty.give.cost);
                    addPoints(user, 'Re', finance.afterParty.give.award);

                    eventType = 'like';
                } else {
                    freebie.numberOfDislikes++;
                    addPoints(freebie, 'Pr', -finance.afterParty.pickUp.cost);
                    addPoints(user, 'Pr', finance.afterParty.pickUp.cost);
                    addPoints(user, 'Re', -finance.afterParty.pickUp.award);

                    eventType = 'dislike';
                }
                addEvent(freebie.id, eventType);
                freebie.save(function (err) {
                    if (err) {
                        log.log('error', 'likeDislikeFreebie: ' + err);
                        return callback('Внутренняя ошибка сервера');
                    }
                    user.save(function (err) {
                        if (err) {
                            log.log('error', 'likeDislikeFreebie: ' + err);
                            return callback('Внутренняя ошибка сервера');
                        }
                        callback(null, [true]);
                    });
                });
            });
        });
    },
    freebieAction: function (user, args, callback) {
        User.findOne({accountID: args.id}, function (err, freebie) {
            if (err) {
                log.log('error', 'freebieAction: ' + err);
                return callback('Внутренняя ошибка сервера');
            }
            if (!freebie) {
                return callback('Пользователь не найден');
            }
            let phanks = false;
            let exist = false;
            user.likeList.forEach(function (f) {
                if (f.accountID == freebie.accountID) {
                    exist = true;
                    return phanks = f.phanks;
                }
            });
            if(!exist){
                return callback('Пользователь не отмечал вас');
            }
            if(phanks){
                return callback('Уже было ответное действие');
            }
            let eventType = 'thanks';
            user.likeList.forEach(function(f) {
                if(f.accountID == freebie.accountID){
                    if(f.like){
                        freebie.Cf += 3;
                        eventType = 'thanks';
                    } else {
                        changeAnimal(freebie, 'pesec');
                        eventType = 'revenge';
                    }
                    f.phanks = true;
                    return true;
                }
            });
            addEvent(freebie.id, eventType);

            freebie.save(function (err) {
                if (err) {
                    log.log('error', 'freebieAction: ' + err);
                    return callback('Внутренняя ошибка сервера');
                }
                user.save(function (err) {
                    if (err) {
                        log.log('error', 'freebieAction: ' + err);
                        return callback('Внутренняя ошибка сервера');
                    }
                    callback(null, [true]);
                });
            });
        });
    },

    //Getters and setters info
    getInfo: function (user, args, callback) {
        let json = [];
        let totalIndex = -1, inCityIndex = -1, referrersIndex = -1, dayQuestIndex = -1, bioTimerIndex = -1,
            awardsIndex = -1, lotteryTypeIndex = -1, likeIndex = -1, eventsIndex = -1,
            favoritesIndex = -1, refAdvertisersIndex = -1, VipDateIndex = -1, prolongationDateIndex = -1;
        let waiting = 0;
        let success = false;

        Finance.findOne({}, function (err, finance) {
            if (err) {
                log.log('error', 'getInfo: ' + err);
                return callback('Внутренняя ошибка сервера');
            }
            if (!finance) {
                return callback('Внутренняя ошибка сервера');
            }

            args.forEach(function (arg, i) {
                if (user[arg] !== undefined) {
                    if (arg === 'dopQuestions') {
                        if (user[arg].length !== 0) {
                            let dopQuestionsArray = [];
                            user[arg].forEach(function (question) {
                                dopQuestionsArray.push(question._id, question.questionState, question.questionText, question.answerText, question.answerSite);
                            });
                            json.push(dopQuestionsArray);
                        } else {
                            json.push(false);
                        }
                    } else if (arg === 'favorites') {
                        if (user.favorites.length !== 0) {
                            waiting++;
                            json.push(false);
                            favoritesIndex = i;
                            let favoritesArray = [];

                            User.findById(user.id)
                                .populate('favorites.companyId')
                                .exec(function (err, user) {
                                    if (err) {
                                        log.log('error', 'getInfo: ' + err)
                                    }
                                    user.favorites.forEach(function (favorite) {
                                        if(favorite.companyId && favorite.companyId.accountID){
                                            favoritesArray.push(favorite._id, favorite.companyId.sphereOfActivity, favorite.companyId.companyName, favorite.companyId.answerSite);
                                        }
                                    });

                                    if (favoritesArray.length !== 0) {
                                        json.splice(favoritesIndex - 1, 1, favoritesArray);
                                    } else {
                                        json.splice(favoritesIndex - 1, 1, false);
                                    }
                                    waiting--;
                                    if (waiting === 0 && success) {
                                        return callback(null, json);
                                    }
                                });
                        } else {
                            json.push(false);
                        }
                    } else if (arg === 'lotteryPoints') {
                        if (user.lotteryState !== 'none') {
                            json.push(user.lotteryPoints);
                        } else {
                            json.push(false);
                        }
                    } else if (arg === 'likeList') {
                        if (user[arg].length !== 0) {
                            waiting++;
                            json.push(false);
                            likeIndex = i;
                            let likeListArray = [];
                            let length = user[arg].length;
                            user[arg].forEach(function (likes) {
                                User.findOne({accountID: likes.accountID}, function (err, us) {
                                    if (err) {
                                        log.log('error', 'getInfo: ' + err);
                                    }
                                    if (us) {
                                        likeListArray.push(likes.accountID, us.Pr, us.Re, likes.like, likes.phanks);
                                    }
                                    length--;
                                    if (length === 0) {
                                        json.splice(likeIndex - 1, 1, likeListArray);

                                        waiting--;
                                        if (waiting === 0 && success) {
                                            return callback(null, json);
                                        }
                                    }
                                });
                            });
                        } else {
                            json.push(false);
                        }
                    } else if (arg === 'awards') {
                        waiting++;
                        json.push(false);
                        awardsIndex = i;
                        User.findById(user.id)
                            .select('awards')
                            .populate('awards.companyId')
                            .exec(function (err, newUser) {
                                if (err) {
                                    return log.log('error', 'getInfo: ' + err)
                                }
                                let awardsArray = [];
                                // let accountIDs = [];
                                if (newUser.awards.length !== 0) {
                                    newUser.awards.sort(function (a, b) {
                                        if (a.currentGt > b.currentGt) {
                                            return -1;
                                        }
                                        if (a.currentGt < b.currentGt) {
                                            return 1;
                                        }
                                        return 0;
                                    });
                                    newUser.awards.forEach(function (award) {
                                        // accountIDs.push(award.companyId.accountID);
                                        if(award.companyId && award.companyId.accountID){
                                            if (award.currentGt > 0 && (award.companyId.VipByGameState || award.companyId.VipByCountryState)) {
                                                awardsArray.push(award.currentGt, award.companyId.VipByGameAward.value, award.companyId.VipByGameAward.type, award.companyId.companyName, award.companyId.answerSite);
                                            }
                                        }
                                    });
                                }
                                if (awardsArray.length !== 0) {
                                    json.splice(awardsIndex - 1, 1, awardsArray);
                                } else {
                                    json.splice(awardsIndex - 1, 1, false);
                                }
                                waiting--;
                                if (waiting === 0 && success) {
                                    return callback(null, json);
                                }

                                // Остальные рекламодатели

                                // Advertiser.find({VipByGameState: true, accountID: {$nin: accountIDs}}, function (err, advertisers) {
                                //     if (err) {
                                //         return log.log('error', 'getInfo: ' + err)
                                //     }
                                //
                                //     advertisers.forEach(function (advertiser) {
                                //         awardsArray.push(0, advertiser.VipByGameAward.value, advertiser.VipByGameAward.type, advertiser.companyName, advertiser.answerSite);
                                //     });
                                //     if (awardsArray.length !== 0) {
                                //         json.splice(awardsIndex - 1, 1, awardsArray);
                                //     } else {
                                //         json.splice(awardsIndex - 1, 1, false);
                                //     }
                                //     waiting--;
                                //     if (waiting === 0 && success) {
                                //         return callback(null, json);
                                //     }
                                // });
                            });
                    } else if (arg === 'motivationTextDate') {
                        if (user.freebie) {
                            json.push(user.motivationTextDate.getTime());
                        } else {
                            json.push(0);
                        }
                    } else if (arg === 'endMoneyDate') {
                        if (user.money > 0) {
                            json.push(user.endMoneyDate.getTime() - Date.now());
                        } else {
                            json.push(0);
                        }
                    } else if (arg === 'dayQuest') {
                        waiting++;
                        json.push(false);
                        dayQuestIndex = i;
                        Lottery.findOne({type: 'dayQuest'}, function (err, lottery) {
                            if (err) {
                                log.log('error', 'getInfo: ' + err);
                            }
                            if (lottery) {
                                json.splice(dayQuestIndex - 1, 1, [user.dayQuest.state, finance.currentQuest.targetType, user.dayQuest.value, finance.currentQuest.targetValue, lottery.date.getTime() - Date.now(), finance.currentQuest.awardType, finance.currentQuest.awardValue]);
                            } else {
                                json.splice(dayQuestIndex - 1, 1, false);
                            }
                            waiting--;
                            if (waiting === 0 && success) {
                                return callback(null, json);
                            }
                        });
                    } else {
                        json.push(user[arg]);
                    }
                } else if (arg === 'bioTimer') {
                    if (user.activeAnimal === 'bio') {
                        json.push(0);
                        waiting++;
                        bioTimerIndex = i;
                        Timer.findOne({userId: user.id, type: 'bio'}, function (err, timer) {
                            if (err) {
                                log.log('error', 'getInfo: ' + err);
                            }
                            if(timer){
                                json.splice(bioTimerIndex - 1, 1, timer.date.getTime() - Date.now());
                            } else {
                                json.splice(bioTimerIndex - 1, 1, 0);
                            }
                            waiting--;
                            if (waiting === 0 && success) {
                                return callback(null, json);
                            }
                        });
                    } else {
                        json.push(0);
                    }
                } else if (arg === 'total') {
                    waiting++;
                    json.push(false);
                    totalIndex = i;
                    User.count({}).exec(function (err, count) {
                        if (err) {
                            log.log('error', 'getInfo: ' + err);
                        }
                        json.splice(totalIndex - 1, 1, 1300+count);
                        waiting--;

                        if (waiting === 0 && success) {
                            return callback(null, json);
                        }
                    });
                } else if (arg === 'inCity') {
                    waiting++;
                    json.push(false);
                    inCityIndex = i;
                    User.count({city: user.city}).exec(function (err, count) {
                        if (err) {
                            log.log('error', 'getInfo: ' + err);
                        }
                        json.splice(inCityIndex - 1, 1, 130+count);
                        waiting--;

                        if (waiting === 0 && success) {
                            return callback(null, json);
                        }
                    });
                } else if (arg === 'prolongationDate') {
                    if (user.activePacket) {
                        json.push(0);
                        waiting++;
                        prolongationDateIndex = i;
                        Timer.findOne({userId: user.id, type: 'prolongation'}, function (err, timer) {
                            if (err) {
                                log.log('error', 'getInfo: ' + err);
                            }
                            if (timer) {
                                json.splice(prolongationDateIndex - 1, 1, timer.date.getTime());
                            }
                            waiting--;
                            if (waiting === 0 && success) {
                                return callback(null, json);
                            }
                        });
                    } else {
                        json.push(0);
                    }
                } else if (arg === 'VipDate') {
                    if (user.VipByCountryState || user.VipByGameState) {
                        json.push(0);
                        waiting++;
                        VipDateIndex = i;
                        Timer.findOne({userId: user.id, type: user.VipType}, function (err, timer) {
                            if (err) {
                                log.log('error', 'getInfo: ' + err);
                            }
                            if (timer) {
                                json.splice(VipDateIndex - 1, 1, timer.date.getTime());
                            }
                            waiting--;
                            if (waiting === 0 && success) {
                                return callback(null, json);
                            }
                        });
                    } else {
                        json.push(0);
                    }
                } else if (arg === 'adAward') {
                    json.push(finance.adAward.cost);
                } else if (arg === 'adSingleAward') {
                    json.push(finance.adSingleAward.cost);
                } else if (arg === 'referrerAward') {
                    json.push(finance.referrerAward.cost);
                } else if (arg === 'exchangeLuToSquirrel') {
                    json.push(finance.exchangeLuck.squirrel.cost);
                } else if (arg === 'exchangeLuToReCost') {
                    json.push(finance.exchangeLuck.Re.cost);
                } else if (arg === 'exchangeLuToReAward') {
                    json.push(finance.exchangeLuck.Re.award);
                } else if (arg === 'exchangeLuToCfCost') {
                    json.push(finance.exchangeLuck.Cf.cost);
                } else if (arg === 'exchangeLuToCfAward') {
                    json.push(finance.exchangeLuck.Cf.award);
                } else if (arg === 'afterPartyGiveCost') {
                    json.push(finance.afterParty.give.cost);
                } else if (arg === 'afterPartyGiveAward') {
                    json.push(finance.afterParty.give.award);
                } else if (arg === 'afterPartyPickUpCost') {
                    json.push(finance.afterParty.pickUp.cost);
                } else if (arg === 'afterPartyPickUpAward') {
                    json.push(finance.afterParty.pickUp.award);
                } else if (arg === 'events') {
                    waiting++;
                    json.push(false);
                    eventsIndex = i;
                    let eventsArray = [];
                    Event.update({userId: user.id}, {read: true}, {multi: true}, function (err) {
                        if (err) {
                            log.log('error', 'getInfo: ' + err);
                        }
                        Event.find({userId: user.id, read: true}).sort('-date').exec(function (err, events) {
                            if (err) {
                                log.log('error', 'getInfo: ' + err);
                            }

                            if (events.length === 0) {
                                json.splice(eventsIndex - 1, 1, false);
                                waiting--;
                                if (waiting === 0 && success) {
                                    return callback(null, json);
                                }
                            } else {
                                let promises = [];
                                events.forEach(function (event) {
                                    if (event.type.search(/money/i) !== -1) {
                                        let typeAndValue = event.type.replace('money', '').split(';');
                                        addPoints(user, typeAndValue[0], +typeAndValue[1]);
                                        eventsArray.push(event.type);
                                    } else {
                                        switch (event.type){
                                            case 'lotteryInvite':
                                                if(user.Pr === 1000){
                                                    promises.push({type: event.type, promise: Lottery.findOne({type: 'lottery'}).exec()});
                                                }
                                                break;
                                            case 'lotteryStart':
                                                if(user.lotteryState === 'lottery'){
                                                    promises.push({type: event.type, promise: Lottery.findOne({type: 'lottery', state: 'game'}).exec()});
                                                }
                                                break;
                                            case 'superLotteryInvite':
                                                if(user.Re === 1000){
                                                    promises.push({type: event.type, promise: Lottery.findOne({type: 'superLottery'}).exec()});
                                                }
                                                break;
                                            case 'superLotteryStart':
                                                if(user.lotteryState === 'superLottery'){
                                                    promises.push({type: event.type, promise: Lottery.findOne({type: 'superLottery', state: 'game'}).exec()});
                                                }
                                                break;
                                            case 'superLotteryCountdownStart':
                                                promises.push({type: event.type, promise: Lottery.findOne({type: 'superLottery', state: 'waiting'}).exec()});
                                                break;
                                            // case 'zeroCf':
                                            //     if(user.Cf === 0) eventsArray.push(event.type);
                                            //     break;
                                            // case 'biohazard':
                                            //     if(user.activeAnimal === 'bio') eventsArray.push(event.type);
                                            //     break;
                                            // case 'hamster':
                                            //     if(user.activeAnimal === 'hamster') eventsArray.push(event.type);
                                            //     break;
                                            // case 'squirrel':
                                            //     if(user.activeAnimal === 'squirrel') eventsArray.push(event.type);
                                            //     break;
                                            // case 'pesec':
                                            //     if(user.activeAnimal === 'pesec') eventsArray.push(event.type);
                                            //     break;
                                            default:
                                                eventsArray.push(event.type);
                                                break;
                                        }
                                    }
                                });
                                Event.remove({userId: user.id, read: true}, function (err) {
                                    if (err) {
                                        log.log('error', 'getInfo: ' + err);
                                    }
                                });
                                let promisesLength = promises.length;
                                if(promisesLength !== 0){
                                    promises.forEach(function (promise) {
                                        promise.promise.then(function (lottery) {
                                            promisesLength--;
                                            if(lottery && lottery.state !== 'pause'){
                                                eventsArray.push(promise.type);
                                            }
                                            if(promisesLength === 0){
                                                json.splice(eventsIndex - 1, 1, eventsArray);
                                                waiting--;
                                                if (waiting === 0 && success) {
                                                    return callback(null, json);
                                                }
                                            }
                                        });
                                    });
                                } else {
                                    json.splice(eventsIndex - 1, 1, eventsArray);
                                    waiting--;
                                    if (waiting === 0 && success) {
                                        return callback(null, json);
                                    }
                                }
                            }
                        });
                    });
                } else if (arg === 'lotteryType') {
                    waiting++;
                    json.push(false);
                    lotteryTypeIndex = i;

                    Lottery.find({}, function (err, lotteries) {
                        if (err) {
                            log.log('error', 'getInfo: ' + err);
                        }

                        let temp = {
                            type: 'none',
                            state: '',
                            date: 0
                        };
                        lotteries.forEach(function (lottery) {
                            if (lottery.type !== 'dayQuest' && lottery.state !== 'pause') {
                                if (!temp.date || temp.date > lottery.date.getTime() - Date.now()) {
                                    temp.type = lottery.type;
                                    temp.state = lottery.state;
                                    temp.date = lottery.date.getTime() - Date.now();
                                }
                            }
                        });

                        json.splice(lotteryTypeIndex - 1, 1, [temp.type, temp.state, temp.date]);
                        waiting--;
                        if (waiting === 0 && success) {
                            return callback(null, json);
                        }
                    });
                } else if (arg === 'referrersAdvertiser') {
                    if (user.referrers.advertiser.length !== 0) {
                        waiting++;
                        json.push(false);
                        refAdvertisersIndex = i;
                        let advertisersArray = [];

                        User.findById(user.id)
                            .populate('referrers.advertiser.companyId')
                            .exec(function (err, user) {
                                if (err) {
                                    return log.log('error', 'getInfo: ' + err)
                                }
                                user.referrers.advertiser.forEach(function (referrer) {
                                    if (referrer.companyId && referrer.companyId.accountID) {
                                        advertisersArray.push(referrer.companyId.accountID, referrer.date.getTime(), referrer.cost, referrer.companyId.companyName);
                                    }
                                });
                                if (advertisersArray.length !== 0) {
                                    json.splice(refAdvertisersIndex - 1, 1, advertisersArray);
                                } else {
                                    json.splice(refAdvertisersIndex - 1, 1, false);
                                }
                                waiting--;
                                if (waiting === 0 && success) {
                                    return callback(null, json);
                                }
                            });
                    } else {
                        json.push(false);
                    }
                } else if (arg === 'referrersUser') {
                    if (user.referrers.user.length !== 0) {
                        waiting++;
                        json.push(false);
                        referrersIndex = i;
                        User.findById(user.id)
                            .populate({path: 'referrers.user', options: {sort: {'Re': -1}}})
                            .exec(function (err, newUser) {
                                if (err) {
                                    return log.log('error', 'getInfo: ' + err)
                                }

                                let jsonArray = [];
                                newUser.referrers.user.forEach(function (referrer) {
                                    jsonArray.push(referrer.accountID, referrer.registerDate.getTime(), referrer.Re, referrer.accountID);
                                });
                                json.splice(referrersIndex - 1, 1, jsonArray);

                                waiting--;
                                if (waiting === 0 && success) {
                                    return callback(null, json);
                                }
                            });
                    } else {
                        json.push(false);
                    }
                } else if (i !== 0) {
                    json.push('Wrong argument');
                }
            });
            success = true;
            if (waiting === 0 && success) {
                callback(null, json);
            }
        });
    },
    infoUpdate: function (user, args, callback) {
        if(!args){
            return callback('Неверный запрос');
        }
        let keys = Object.keys(args);
        let json = [];
        keys.forEach(function (key) {
            if (user[key] !== undefined || key === 'password') {
                switch (key) {
                    case 'country':
                    case 'city':
                    case 'email':
                    case 'password':
                    case 'outFIO':
                    case 'outBankName':
                    case 'outINN':
                    case 'outKPP':
                    case 'outBIK':
                    case 'outBankAccount':
                    case 'outCorAccount':
                    case 'motivationText':
                        if(key == 'email'){
                            let re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
                            if (re.test(args[key])){
                                json.push(true);
                                user.email = args[key];
                            } else {
                                json.push(false);
                            }
                        } else {
                            json.push(true);
                            user[key] = args[key];
                        }
                        break;
                    case 'sphereOfActivity':
                    case 'companyName':
                    case 'questionState':
                    case 'questionText':
                    case 'answerText':
                    case 'answerSite':
                    case 'VipText':
                    case 'FIO':
                    case 'bankName':
                    case 'INN':
                    case 'KPP':
                    case 'BIK':
                    case 'bankAccount':
                    case 'corAccount':
                        if (user._type === 'advertiser') {
                            json.push(true);
                            user[key] = args[key];
                        } else {
                            json.push(false);
                            log.log('info', 'Недостаточно прав')
                        }
                        break;
                    case 'Re':
                    case 'Pr':
                    case 'Lu':
                    case 'Al':
                    case 'accountID':
                        if (user._type !== 'administrator') {
                            json.push(true);
                            user[key] = args[key];
                        } else {
                            json.push(false);
                            log.log('info', 'Недостаточно прав')
                        }
                        break;
                    default:
                        json.push(false);
                        break;
                }
            } else {
                json.push(false);
            }
        });
        user.save(function (err) {
            if (err) {
                log.log('error', 'infoUpdate: ' + err);
            }
            return callback(null, json);
        });
    },

    //Question section
    getQuestion: function (user, args, callback) {
        if(args.type !== 'question5' && args.type !== 'question15' && args.type !== 'question1'){
            return callback('Неверный тип');
        }
        if(args.type == 'question1'){
            Advertiser.findOne({_id: args.advertiserID, activePacket:true, questionState: 'ok'}, function (err, advertiser) {
                if (err) {
                    log.log('error', 'getQuestion: ' + err);
                    return callback('Внутренняя ошибка сервера');
                }

                if (!advertiser) {
                    return callback('Вопрос не найден');
                    // let country = true;
                    // let query = {_type: 'advertiser', activePacket: true, country: user.country, questionState: 'ok'};
                    // if(Math.random() > 0.5){
                    //     country = false;
                    //     query.city = user.city;
                    // } else {
                    //     query.VipByCountryState = true;
                    // }
                    //
                    // getRandomQuestion(user, args.type, query, function (err, result) {
                    //     if(err){
                    //         if (!country) {
                    //             getRandomQuestion(user, args.type, {_type: 'advertiser', activePacket: true, country: user.country, VipByCountryState: true, questionState: 'ok'}, function (err, result) {
                    //                 callback(err, result);
                    //             });
                    //         } else {
                    //             getRandomQuestion(user, args.type, {_type: 'advertiser', activePacket: true, country: user.country, city: user.city, questionState: 'ok'}, function (err, result){
                    //                 callback(err, result);
                    //             });
                    //         }
                    //     } else {
                    //         callback(err, result);
                    //     }
                    // });
                } else {
                    let questions = [];
                    let maxQuestionsNumber = 1;
                    advertiser.dopQuestions.forEach(function (q, i) {
                        if (q.questionState === 'ok') {
                            questions.push(i);
                            maxQuestionsNumber++;
                        }
                    });

                    let exist = false;
                    let advertiserAward;
                    user.awards.forEach(function (award) {
                        if (award.companyId && award.companyId.toString() === advertiser.id) {
                            if(award.questionNumber >= maxQuestionsNumber){
                                award.questionNumber = 0;
                                user.save(function (err) {
                                    if(err){
                                        log.log('error', 'getQuestion: ' + err);
                                    }
                                });
                            }
                            advertiserAward = award;
                            exist = true;
                        }
                    });
                    if (!exist) {
                        advertiserAward = {
                            companyId: advertiser.id,
                            currentGt: 0,
                            questionNumber: 0
                        };
                        user.awards.push(advertiserAward);
                    }

                    let questionNumber = advertiserAward.questionNumber;
                    let question = new Answer({
                        userId: user.id,
                        advertiserId: advertiser.id,
                        answerText: questionNumber == 0 ? advertiser.answerText: advertiser.dopQuestions[questions[questionNumber - 1]].answerText,
                        questionNumber: questionNumber===0?0:(questions[questionNumber - 1]+1),
                        questionType: 'question1',
                        createdAt: Date.now()
                    });
                    question.save(function (err) {
                        if (err) {
                            log.log('error', 'getQuestion: ' + err);
                            return callback('Внутренняя ошибка сервера');
                        }
                        if (questionNumber === 0) {
                            return callback(null, [question.id, advertiser.questionText, advertiser.answerText, advertiser.answerSite, maxQuestionsNumber-questionNumber]);
                        } else {
                            return callback(null, [question.id, advertiser.dopQuestions[questions[questionNumber - 1]].questionText, advertiser.dopQuestions[questions[questionNumber - 1]].answerText, advertiser.dopQuestions[questions[questionNumber - 1]].answerSite, maxQuestionsNumber-questionNumber]);
                        }
                    });
                }
            });
        } else {
            if (user.questionsState[args.type] === 0) {
                createAdvertisersList(user, args.type, function () {
                    getRandomQuestion(user, 0, args.type, callback);
                });
            } else {
                let number = user.questionsState[args.type]%user.questionsState.advertisers.length;
                getRandomQuestion(user, number, args.type, callback);
            }
        }
    },
    addToFavorites: function (user, args, callback) {
        Answer.findById(args.answerId).exec(function (err, question) {
            if (err) {
                log.log('error', 'addToFavorites: ' + err);
                return callback('Внутренняя ошибка сервера');
            }

            if (!question) {
                return callback('Вопрос не найден');
            }

            User.findOneAndUpdate({
                accountID: user.accountID,
                'favorites.companyId': {$ne: question.advertiserId}
            }, {"$push": {favorites: {companyId: question.advertiserId}}}, function (err, user) {
                if (err) {
                    log.log('error', 'addToFavorites: ' + err);
                    return callback('Внутренняя ошибка сервера');
                }
                if (!user) {
                    return callback('Уже в избранном');
                }
                callback(null, [true]);
            });
        });
    },
    removeFavorite: function (user, args, callback) {
        user.favorites.pull(args.id);
        user.save(function (err) {
            if (err) {
                log.log('error', 'removeFavorite: ' + err);
                return callback('Внутренняя ошибка сервера');
            }
            callback(null, [true]);
        });
    },
    checkAnswer: function (user, args, callback) {
        Answer.findById(args.answerId, function (err, question) {
            if (!question) {
                return callback(null, [+args.answerText >= 0]);
            }

            let result = question.answerText === args.answerText;
            Answer.findOneAndUpdate({_id: args.answerId, answered: false}, {type: result, answered: true}).populate('advertiserId').exec(function (err, question) {
                if (err) {
                    return log.log('error', 'checkAnswer: ' + err);
                }
                if (!question || !question.advertiserId) {
                    return callback(null, [+args.answerText >= 0]);
                }
                let advertiser = question.advertiserId;
                if (advertiser) {
                    question.questionNumber === 0 ? advertiser.totalAnswers++ : advertiser.dopQuestions[question.questionNumber - 1].totalAnswers++;
                    if (result) {
                        Finance.findOne({}, function (err, finance) {
                            if (err) {
                                return log.log('info', err);
                            }
                            if (finance) {
                                if (question.questionType !== 'question1') {
                                    addPoints(user, 'Re', finance.adAward.cost);
                                } else {
                                    addPoints(user, 'Re', finance.adSingleAward.cost);
                                }
                            }
                        });
                    } else {
                        question.questionNumber === 0 ? advertiser.falseAnswers++ : advertiser.dopQuestions[question.questionNumber - 1].falseAnswers++;
                    }
                    if (question.questionType === 'question1') {
                        let maxQuestionsNumber = 1;
                        advertiser.dopQuestions.forEach(function (q) {
                            if (q.questionState === 'ok') {
                                maxQuestionsNumber++;
                            }
                        });
                        user.awards.forEach(function (award) {
                            if (award.companyId.toString() === advertiser.id) {
                                award.questionNumber++;
                                if (award.questionNumber >= maxQuestionsNumber) {
                                    award.questionNumber = 0;
                                }
                            }
                        });
                    } else if (question.questionType === 'question5') {
                        user.questionsState.question5++;
                        if (user.questionsState.question5 >= 5) {
                            user.questionsState.question5 = 0;
                            user.Cf += 1;
                        }
                    } else if (question.questionType === 'question15') {
                        user.questionsState.question15++;
                        if (user.questionsState.question15 >= 15) {
                            user.questionsState.question15 = 0;

                            if(user.Cf > 0 && user.activeAnimal != 'bio'){
                                user.dayQuest.dayAnswers++;
                                if (user.dayQuest.dayAnswers >= 10) {
                                    addEvent(user.id, 'biohazard');
                                    user.dayQuest.dayAnswers = 0;
                                    user.activeAnimal = 'bio';
                                    addTimer(user.id, 'bio', 3600 * 1000);
                                }
                            }
                            user.Cf += 3;
                            switch (user.star) {
                                case 0:
                                    user.Cf += 3;
                                    break;
                                case 1:
                                    user.Cf += 2;
                                    break;
                                case 2:
                                    user.Cf += 1;
                                    break;
                                default:
                                    break;
                            }
                        }
                    }

                    if(question.questionType === 'question5' || question.questionType === 'question15'){
                        let number = user.questionsState[question.questionType]%user.questionsState.advertisers.length;
                        if(user.questionsState.advertisers[number]) {
                            if(!user.questionsState.advertisers[number].questionNumber){
                                user.questionsState.advertisers[number].questionNumber = 0;
                            }
                            user.questionsState.advertisers[number].questionNumber++;
                        }
                    }
                    user.save(function (err) {
                        if (err) {
                            log.log('error', 'checkAnswer: ' + err);
                        }
                    });
                    advertiser.save(function (err) {
                        if (err) {
                            log.log('error', 'checkAnswer: ' + err);
                        }
                    });
                }
            });
            return callback(null, [result]);
        });
    },

    //Game section
    playGame: function (user, args, callback) {
        if (args.type == 'lottery' || args.type == 'superLottery') {
            Lottery.findOne({type: args.type}, function (err, lottery) {
                if (err) {
                    log.log('error', 'playGame: ' + err);
                    return callback('Внутренняя ошибка сервера');
                }
                if (!lottery || lottery.state !== 'game') {
                    return callback('Игра ещё не началась');
                }

                User.findOneAndUpdate({_id: user.id, lotteryNumber: {"$lt": 3}},  {$inc: {lotteryNumber: 1}}, function (err, user) {
                    if(err){
                        log.log('error', 'playGame: ' + err);
                        return callback('Внутренняя ошибка сервера');
                    }
                    if(!user){
                        return callback('Уже сыграл в розыгрыше');
                    }

                    if (user.lotteryState !== args.type) {
                        return callback('Сначала присоединитесь к розыгрышу');
                    }
                    playLotteryGame(user, lottery, function (err, result) {
                        callback(err, result);
                    });
                });
            });
        } else {
            playSimpleGame(user, function (err, result) {
                callback(err, result);
            });
        }
    },
    getGame: function (user, args, callback) {
        if (args.type) {
            return callback(null, [roulette.hamster.values, roulette.top.dop]);
        }
        return callback(null, [user.activeAnimal, roulette.hamster.values, roulette[user.activeAnimal].types]);
    },
    getExchangeCosts: function (user, args, callback) {
        if(!args || args.length === 0){
            return callback('Неверный запрос');
        }
        Finance.findOne({}, function (err, finance) {
            if (err) {
                log.log('error', 'getExchangeCosts: ' + err);
                return callback('Внутренняя ошибка сервера');
            }
            if(!finance){
                return callback('Ошибка сервера');
            }
            let resultArray = [];
            args.forEach(function (arg) {
                if(finance.exchangeLuck[arg] && finance.exchangeLuck[arg].cost){
                    if(arg !== 'squirrel') {
                        resultArray.push([finance.exchangeLuck[arg].cost, finance.exchangeLuck[arg].award]);
                    } else {
                        resultArray.push([finance.exchangeLuck[arg].cost]);
                    }
                } else {
                    resultArray.push([0]);
                }
            });

            callback(null, resultArray.length === 0 ? [false]: resultArray);
        });
    },
    exchangeLuck: function (user, args, callback) {
        if (args.type !== 'Re' && args.type !== 'Cf' && args.type !== 'squirrel') {
            return callback('Неверный тип');
        }
        Finance.findOne({}, function (err, finance) {
            if (err) {
                log.log('error', 'exchangeLuck: ' + err);
                return callback('Внутренняя ошибка сервера');
            }
            if(!finance){
                return callback('Ошибка сервера');
            }
            if (user.Lu < finance.exchangeLuck[args.type].cost) {
                return callback('Недостаточно Lu');
            }
            user.Lu -= finance.exchangeLuck[args.type].cost;
            if(args.type !== 'squirrel'){
                addPoints(user, args.type, +finance.exchangeLuck[args.type].award);
            } else {
                changeAnimal(user, 'hamster');
            }
            user.save(function (err) {
                if (err) {
                    log.log('error', 'exchangeLuck: ' + err);
                    return callback('Внутренняя ошибка сервера');
                }
                callback(null, [true]);
            });
        });
    },
    getAdInGame: function (user, callback) {
        let inCity = Math.random();
        let country = true;
        if (inCity > 0.4) {
            country = false;
        }
                if (country) {
                    getRandomAd(user, {VipByCountryState: true, country: user.country}, function (err, result) {
                        if (err) {
                            getRandomAd(user, {superType: true}, function (err, result) {
                                callback(err, result);
                            });
                        } else {
                            callback(err, result);
                        }
                    });
                } else {
                    getRandomAd(user, {VipByGameState: true, country: user.country, city: user.city}, function (err, result){
                        if(err){
                            getRandomAd(user, {superType: true}, function (err, result) {
                                callback(err, result);
                            });
                        } else {
                            callback(err, result);
                        }
                    });
                }
    },

    feedback: function (user, args, callback) {
        Victory.findOneAndUpdate({accountID: user.accountID, verifyUser: false}, {verifyUser: true}, function (err, victory) {
            if(err){
                log.log('error', 'feedback: ' + err);
                return callback('Внутренняя ошибка сервера');
            }
            if(!victory){
                return callback('Вы уверены в победе?')
            }

            args.accountID = user.accountID;
            args.email = user.email;
            args.city = user.city;
            args.country = user.country;

            Administrator.findOne({_type: 'administrator'}, function (err, admin) {
                if (err) {
                    return log.log('error', 'feedback: ' + err);
                }
                mailer.sendMail({
                    theme: 'feedback',
                    address: admin.email,
                    args: args,
                    subject: 'Данные победителя'
                }, function (err) {
                    if (err) {
                        return log.log('error', 'Ошибка при отправке сообщения');
                    }
                });
            });
            // mailer.sendMail({
            //     theme: 'feedback',
            //     address: user.email,
            //     args: args,
            //     subject: 'Данные победителя'
            // }, function (err) {
            //     if (err) {
            //         return log.log('error', 'Ошибка при отправке сообщения');
            //     }
            // });
            callback(null, [true]);
        });
    },

    //Lottery section
    joinLottery: function (user, args, callback) {
        if (args.type !== 'superLottery' && args.type !== 'lottery') {
            return callback('Неверный тип розыгрыша');
        }
        if (user.lotteryState !== 'none') {
            return callback('Уже присоединился к розыгрышу');
        }
        if (args.type === 'lottery') {
            if (user.Pr < 1000) {
                return callback('Недостаточно Pr');
            }
        } else {
            if (user.Re < 1000) {
                return callback('Недостаточно Re');
            }
        }
        let self = this;
        User.findOneAndUpdate({_id: user.id, lotteryState: 'none'}, {lotteryState: args.type, lotteryDate: Date.now(), lotteryNumber: 0, lotteryPoints: [0, 0, 0]}, function (err, user) {
            if (err) {
                log.log('error', 'joinLottery: ' + err);
                return callback('Внутренняя ошибка сервера');
            }
            if (!user) {
                return callback('Уже присоединился к розыгрышу');
            }
            self.leaveParty(user, function () {
                callback(null, [true]);
            });
        });
    },
    getLotteryInfo: function (user, args, callback) {
        if(args.type !== 'lottery' && args.type !== 'superLottery'){
            return callback('Неверный тип');
        }
        Lottery.findOne({type: args.type}, function (err, lottery) {
            if (err){
                log.log('error', 'getLotteryInfo: ' + err);
                return callback('Внутренняя ошибка сервера');
            }
            if(!lottery){
                return callback('Розыгрыш не найден');
            }
            let array = [];
            let ourUser = {
                place: 0,
                points: 0
            };
            LotteryGame.find({lotteryType: lottery.type}, null, {sort: {'points': -1}}, function (err, players) {
                if (err){
                    log.log('error', 'getLotteryInfo: ' + err);
                    return callback('Внутренняя ошибка сервера');
                }
                players.forEach(function(u, i){
                    if(i < 50) {
                        array.push(u.points, u.accountID);
                    }
                    if(u.accountID === user.accountID){
                        ourUser.place = i + 1;
                        ourUser.points = u.points;
                    }
                });
                callback(null, [args.type === user.lotteryState, lottery.state, lottery.date.getTime() - Date.now(), lottery.text, lottery.url, user.Re, ourUser.points, ourUser.place, array]);
            });
        });
    },
    getLotteryGameInfo: function (user, args, callback) {
        if(args.type !== 'lottery' && args.type !== 'superLottery'){
            return callback('Неверный тип розыгрыша');
        }
        if(args.type !== user.lotteryState){
            return callback('Сначала присоединитесь к розыгрышу');
        }
        Lottery.findOne({type: args.type}, function (err, lottery) {
            if (err) {
                log.log('error', 'getLotteryGameInfo: ' + err);
                return callback('Внутренняя ошибка сервера');
            }
            if (!lottery) {
                return callback('Розыгрыш не найден');
            }
            let array = [];
            let ourUser = {place: 0, points: 0};
            LotteryGame.find({lotteryType: lottery.type}, null, {sort: {'points': -1}}, function (err, players) {
                if (err) {
                    log.log('error', 'getLotteryGameInfo: ' + err);
                    return callback('Внутренняя ошибка сервера');
                }
                if (players.length === 0) {
                    array = false;
                }
                players.forEach(function (u, i) {
                    if (i < 5) {
                        array.push(u.points, u.accountID);
                    }
                    if (u.accountID === user.accountID) {
                        ourUser.place = i + 1;
                        ourUser.points = u.points;
                    }
                });
                callback(null, [user.lotteryNumber, lottery.state, lottery.date.getTime() - Date.now(), user.Re, ourUser.points, ourUser.place, array.length === 0 ? false : array]);
            });
        });
    },

    moneyRequest: function (user, callback) {
        if (user.money < 500) {
            return callback('Ошибка запроса.\nВы не можете вывести со счета сумму менее 500 Р.');
        }

        User.findById(user.id).populate('referrers.advertiser.companyId').exec(function (err, user) {
            if (err) {
                log.log('error', 'moneyRequest: ' + err);
                return callback('Внутренняя ошибка сервера');
            }
            let moneyRequest = new MoneyRequest({
                userId: user.id,
                accountID: user.accountID,
                money: user.money,
                createdAt: Date.now()
            });
            let referrersString = '';
            user.referrers.advertiser.forEach(function (ref) {
                if (ref.companyId && ref.companyId.accountID) {
                    referrersString += ref.companyId.companyName;
                    let checkString = '';
                    ref.checks.forEach(function (check, i, checks) {
                        checkString += '#' + check;
                        if(i !== checks.length - 1){
                            checkString += ',';
                        }
                    });
                    referrersString += '\\' + checkString;
                    referrersString += '\\' + ref.cost + 'Р.\n';
                }
            });
            User.update({_id: user.id}, {money: 0, 'referrers.advertiser': []}, function (err, user) {
                if (err) {
                    log.log('error', 'moneyRequest: ' + err);
                    return callback('Внутренняя ошибка сервера');
                }
                moneyRequest.save(function (err) {
                    if (err) {
                        log.log('error', 'moneyRequest: ' + err);
                        return callback('Внутренняя ошибка сервера');
                    }
                    Administrator.findOne({_type: 'administrator'}, function (err, admin) {
                        if (err) {
                            return log.log('error', 'moneyRequest: ' + err);
                        }
                        mailer.sendMail({
                            theme: 'order_money',
                            address: admin.email,
                            referrers: referrersString,
                            user: user,
                            subject: 'Запрос на выплату'
                        }, function (err) {
                            if (err) {
                                return log.log('error', 'Ошибка при отправке сообщения');
                            }
                        });
                    });
                    // mailer.sendMail({
                    //     theme: 'order_money',
                    //     address: user.email,
                    //     referrers: referrersString,
                    //     user: user,
                    //     subject: 'Запрос на выплату'
                    // }, function (err) {
                    //     if (err) {
                    //         return log.log('error', 'Ошибка при отправке сообщения');
                    //     }
                    // });
                    callback(null, [true]);
                });
            });
        });
    },

    //Admin section
    getMoneyRequests: function (administrator, callback) {
        if(administrator._type !== 'administrator'){
            return callback('Ошибка доступа');
        }

        MoneyRequest.find({}, function (err, requests) {
            if (err) {
                log.log('error', 'getMoneyRequests: ' + err);
                return callback('Внутренняя ошибка сервера');
            }
            if (requests.length === 0) {
                return callback('Запросы не найдены');
            }
            let array = [];
            requests.forEach(function (request) {
                array.push(request.id, request.accountID, request.money, request.date.getTime());
            });
            callback(null, array);
        });
    },
    activateMoney: function (administrator, args, callback) {
        if (administrator._type !== 'administrator') {
            return callback('Ошибка доступа');
        }

        MoneyRequest.findByIdAndRemove(args.id, function (err, request) {
            if (err) {
                log.log('error', 'activateMoney: ' + err);
                return callback('Внутренняя ошибка сервера');
            }
            if (!request) {
                return callback('Запрос не найден');
            }
            User.findByIdAndUpdate(request.userId, {plusMoney: true, lastPlusDate: Date.now()}, function (err, user) {
                if (err) {
                    log.log('error', 'activateMoney: ' + err);
                    return callback('Внутренняя ошибка сервера');
                }
                if (!user) {
                    return callback('Пользователь не найден');
                }

                mailer.sendMail({
                    theme: 'mail_template',
                    sub_theme: 'activate_money',
                    address: user.email,
                    money: request.money,
                    currency: 'рублей',
                    subject: 'Вывод средств'
                }, function (err) {
                    if (err) {
                        return log.log('error', 'Ошибка при отправке сообщения');
                    }
                });

                callback(null, [true]);
            });
        });
    },
    getMoneyRequestInfo: function (administrator, args, callback) {
        if(administrator._type !== 'administrator'){
            return callback('Ошибка доступа');
        }

        MoneyRequest.findById(args.requestId).populate('userId', 'outFIO outBankName outBIK outBankAccount outINN outCorAccount').exec(function (err, request) {
            if (err) {
                log.log('error', 'getMoneyRequestInfo: ' + err);
                return callback('Внутренняя ошибка сервера');
            }
            if (!request) {
                return callback('Запрос не найден');
            }
            let user = request.userId;
            if (!user) {
                return callback('Пользователь не найден');
            }
            callback(null, [user.outFIO, user.outBankName, user.outBIK, user.outBankAccount, user.outINN, user.outCorAccount]);
        });
    },

    getParameters: function (administrator, userUpdate, accountID, parameters, callback) {
        if(administrator._type !== 'administrator' || (!parameters || parameters.length == 0)){
            return callback('Ошибка доступа');
        }
        if(userUpdate){
            User.findOne({accountID: accountID}, function (err, user) {
                if(err){
                    log.log('error', 'getParameters: ' + err);
                    return callback('Внутренняя ошибка сервера');
                }
                if(!user){
                    return callback('Пользователь не найден');
                }
                let array = [];
                parameters.forEach(function (parameter) {
                    if(user[parameter] || user[parameter] === 0){
                        array.push(user[parameter]);
                    } else {
                        array.push(false);
                    }
                });
                return callback(null, array);
            });
        } else {
            Finance.findOne({}, function (err, finance) {
                if(err){
                    log.log('error', 'getParameters: ' + err);
                    return callback('Внутренняя ошибка сервера');
                }
                if(!finance){
                    return callback('Внутренняя ошибка сервера');
                }
                let array = [];
                parameters.forEach(function (parameter) {
                    switch (parameter){
                        case 'exchangeLuToSquirrel':
                            array.push(finance.exchangeLuck.squirrel.cost);
                            break;
                        case 'exchangeLuToReCost':
                            array.push(finance.exchangeLuck.Re.cost);
                            break;
                        case 'exchangeLuToReAward':
                            array.push(finance.exchangeLuck.Re.award);
                            break;
                        case 'exchangeLuToCfCost':
                            array.push(finance.exchangeLuck.Cf.cost);
                            break;
                        case 'exchangeLuToCfAward':
                            array.push(finance.exchangeLuck.Cf.award);
                            break;
                        case 'afterPartyGiveCost':
                            array.push(finance.afterParty.give.cost);
                            break;
                        case 'afterPartyGiveAward':
                            array.push(finance.afterParty.give.award);
                            break;
                        case 'afterPartyPickUpCost':
                            array.push(finance.afterParty.pickUp.cost);
                            break;
                        case 'afterPartyPickUpAward':
                            array.push(finance.afterParty.pickUp.award);
                            break;
                        case 'lotteryText':
                            array.push(finance.lottery.text);
                            break;
                        case 'lotteryUrl':
                            array.push(finance.lottery.url);
                            break;
                        case 'currentLotteryText':
                            array.push(finance.lottery.currentText);
                            break;
                        case 'currentLotteryUrl':
                            array.push(finance.lottery.currentUrl);
                            break;
                        default:
                            if(finance[parameter]){
                                array.push(finance[parameter].cost);
                            } else {
                                array.push('Wrong argument');
                            }
                            break;
                    }
                });
                return callback(null, array);
            });
        }
    },
    updateParameters: function (administrator, userUpdate, parameters, callback) {
        if(administrator._type !== 'administrator' || (!parameters[1] || parameters[1].length == 0)){
            return callback('Ошибка доступа');
        }

        if(userUpdate) {
            let query = {};
            parameters[1].forEach(function (parameter, i) {
                query[parameter] = parameters[2][i];
            });
            User.findOneAndUpdate({accountID: parameters[4]}, query, {new: true}, function (err, user) {
                if (err) {
                    log.log('error', 'updateParameters: ' + err);
                    return callback('Внутренняя ошибка сервера');
                }
                if (!user) {
                    return callback('Пользователь не найден');
                }

                addEvent(user.id, 'externalUpdate');
                checkOnStar(user, 'Re', query['Re']);
                checkOnStar(user, 'Pr', query['Pr']);
                return callback(null, [true]);
            });
        } else {
            Finance.findOne({}, function (err, finance) {
                if(err){
                    log.log('error', 'updateParameters: ' + err);
                    return callback('Внутренняя ошибка сервера');
                }
                if(!finance){
                    return callback('Внутренняя ошибка сервера');
                }
                let array = [];
                parameters[1].forEach(function (parameter, i) {
                    let check = true;
                    switch (parameter){
                        case 'exchangeLuToSquirrel':
                            finance.exchangeLuck.squirrel.cost = parameters[2][i];
                            break;
                        case 'exchangeLuToReCost':
                            finance.exchangeLuck.Re.cost = parameters[2][i];
                            break;
                        case 'exchangeLuToReAward':
                            finance.exchangeLuck.Re.award = parameters[2][i];
                            break;
                        case 'exchangeLuToCfCost':
                            finance.exchangeLuck.Cf.cost = parameters[2][i];
                            break;
                        case 'exchangeLuToCfAward':
                            finance.exchangeLuck.Cf.award = parameters[2][i];
                            break;
                        case 'afterPartyGiveCost':
                            finance.afterParty.give.cost = parameters[2][i];
                            break;
                        case 'afterPartyGiveAward':
                            finance.afterParty.give.award = parameters[2][i];
                            break;
                        case 'afterPartyPickUpCost':
                            finance.afterParty.pickUp.cost = parameters[2][i];
                            break;
                        case 'afterPartyPickUpAward':
                            finance.afterParty.pickUp.award = parameters[2][i];
                            break;
                        case 'currentLotteryText':
                            finance.lottery.currentText = parameters[2][i];
                            Lottery.findOneAndUpdate({type: 'lottery'}, {text: finance.lottery.text}, function (err) {
                                if(err){
                                    return log.log('error', 'updateParameters: ' + err);
                                }
                            });
                            break;
                        case 'currentLotteryUrl':
                            finance.lottery.currentUrl = parameters[2][i];
                            Lottery.findOneAndUpdate({type: 'lottery'}, {url: finance.lottery.url}, function (err) {
                                if(err){
                                    return log.log('error', 'updateParameters: ' + err);
                                }
                            });
                            break;
                        case 'lotteryText':
                            finance.lottery.text  = parameters[2][i];
                            break;
                        case 'lotteryUrl':
                            finance.lottery.url = parameters[2][i];
                            break;
                        default:
                            if(finance[parameter]){
                                finance[parameter].cost = parameters[2][i];
                            } else {
                                check = false;
                            }
                            break;
                    }
                    array.push(check);
                });
                finance.save(function (err) {
                    if(err){
                        log.log('error', 'updateParameters: ' + err);
                        return callback('Внутренняя ошибка сервера');
                    }
                    return callback(null, array);
                });
            });
        }
    },

    getLotteryPrizes: function (administrator, callback) {
        if (administrator._type !== 'administrator') {
            return callback('Ошибка доступа');
        }

        Lottery.findOne({type: 'superLottery'}, function (err, lottery) {
            if (err){
                log.log('error', 'getLotteryPrizes: ' + err);
                return callback('Внутренняя ошибка сервера');
            }
            if(!lottery){
                return callback('Рызыгрыш не найден');
            }
            callback(null, [lottery.state, lottery.text, lottery.url, lottery.date.getTime()-Date.now()]);
        });
    },
    setLotteryPrizes: function (administrator, args, callback) {
        if (administrator._type !== 'administrator') {
            return callback('Ошибка доступа');
        }

        Lottery.findOneAndUpdate({type: 'superLottery'}, {text: args.text, url: args.url, date: new Date(Date.now() + +args.time)}, function (err, lottery) {
            if (err) {
                log.log('error', 'setLotteryPrizes: ' + err);
                return callback('Внутренняя ошибка сервера');
            }
            if(!lottery){
                return callback('Розыгрыш не найден');
            }
            callback(null, [true]);
        });
    },

    getAllCheckouts: function (administrator, callback) {
        if (administrator._type !== 'administrator') {
            return callback('Ошибка доступа');
        }
        Checkout.find({}, function (err, checkouts) {
            if (err) {
                log.log('error', 'getAllCheckouts: ' + err);
                return callback('Внутренняя ошибка сервера');
            }
            if (checkouts.length !== 0) {
                let array = [];
                checkouts.forEach(function (checkout) {
                    array.push(checkout.checkoutNumber, checkout.advertiserId);
                });
                callback(null, array);
            } else {
                callback('Запросов на оплату не найдено');
            }
        });
    },
    getCheckoutForUser: function (administrator, args, callback) {
        if (administrator._type !== 'administrator') {
            return callback('Ошибка доступа');
        }
        Checkout.find({advertiserId: args.id}, function (err, checkouts) {
            if (err) {
                log.log('error', 'getCheckoutForUser: ' + err);
                return callback('Внутренняя ошибка сервера');
            }
            if (checkouts.length !== 0) {
                let array = [];
                checkouts.forEach(function (checkout) {
                    array.push(checkout.checkoutNumber);
                });
                callback(null, array);
            } else {
                callback('Запросов на оплату не найдено');
            }
        });
    },
    getCheckoutInfo: function (administrator, args, callback) {
        if (administrator._type !== 'administrator') {
            return callback('Ошибка доступа');
        }
        Checkout.findOne({checkoutNumber: args.checkoutNumber}).populate('advertiserId').exec(function (err, checkout) {
            if (err) {
                log.log('error', 'getCheckoutInfo: ' + err);
                return callback('Внутренняя ошибка сервера');
            }
            if (!checkout) {
                return callback('Запросов на оплату не найдено');
            }
            let advertiser = checkout.advertiserId;
            if (!advertiser) {
                return callback('Пользователь не найден');
            }
            return callback(null, [advertiser.accountID, checkout.cost, advertiser.FIO, advertiser.companyName]);
        });
    },
    setSuperType: function (administrator, args, callback) {
        if (administrator._type !== 'administrator') {
            return callback('Ошибка доступа');
        }
        Advertiser.findOneAndUpdate({accountID: args.id}, {superType: args.type}, function (err, advertiser) {
            if (err) {
                log.log('error', 'setSuperType: ' + err);
                return callback('Внутренняя ошибка сервера');
            }
            if (!advertiser) {
                return callback('Пользователь не найден');
            }
            callback(null, [true]);
        });
    },
    activateOptions: function (administrator, args, callback) {
        if (administrator._type !== 'administrator') {
            return callback('Ошибка доступа');
        }
        Checkout.findOne({checkoutNumber: args.checkoutNumber}).populate('advertiserId').exec(function (err, checkout) {
            if (err) {
                log.log('error', 'activateOptions: ' + err);
                return callback('Внутренняя ошибка сервера');
            }
            if (!checkout) {
                return callback('Запрос на оплату не найден');
            }
            let advertiser = checkout.advertiserId;
            if (!advertiser) {
                return callback('Пользователь не найден');
            }
            let themes = [];
            Finance.findOne({}, function (err, finance) {
                if (err) {
                    log.log('error', 'activateOptions: ' + err);
                    return callback('Внутренняя ошибка сервера');
                }
                if (!finance) {
                    log.log('error', 'activateOptions: ' + err);
                    return callback('Внутренняя ошибка сервера');
                }
                let cost = 0;
                let prolongationUpdate = 0;
                let VipByGameUpdate = 0;
                checkout.options.forEach(function (option) {
                    switch (option) {
                        case 'VipByGame':
                            themes.push('VIP в игре');
                            advertiser.activePacket = true;
                            prolongationUpdate += finance.VipByGame.date * 24 * 3600 * 1000;

                            if (!advertiser.VipByCountryState) {
                                advertiser.VipType = 'VipByGame';
                            }
                            advertiser.VipByGameState = true;
                            VipByGameUpdate += finance.VipByGame.date * 24 * 3600 * 1000;

                            cost += finance.VipByGame.cost;
                            break;
                        case 'VipByCountry':
                            themes.push('VIP по стране');
                            advertiser.activePacket = true;
                            prolongationUpdate += finance.VipByCountry.date * 24 * 3600 * 1000;
                            VipByGameUpdate += finance.VipByCountry.date * 24 * 3600 * 1000;

                            advertiser.VipType = 'VipByCountry';
                            advertiser.VipByCountryState = true;
                            addTimer(advertiser.id, 'VipByCountry', finance.VipByCountry.date * 24 * 3600 * 1000);

                            cost += finance.VipByCountry.cost;
                            break;
                        case 'startPacket':
                            themes.push('Стартовый');
                            advertiser.packetType = 'Стартовый';
                            advertiser.activePacket = true;
                            prolongationUpdate += finance.startPacket.date * 24 * 3600 * 1000;

                            cost += finance.packet30.cost;
                            break;
                        case 'prolongationCost0':
                            themes.push('Пролонгация 30 дней');
                            advertiser.packetType = 'Пролонгация 30 дней';
                            advertiser.activePacket = true;
                            prolongationUpdate += finance.packet30.date * 24 * 3600 * 1000;

                            cost += finance.packet30.cost;
                            break;
                        case 'prolongationCost1':
                            themes.push('Пролонгация 90 дней');
                            advertiser.packetType = 'Пролонгация 90 дней';
                            advertiser.activePacket = true;
                            prolongationUpdate += finance.packet90.date * 24 * 3600 * 1000;

                            cost += finance.packet90.cost;
                            break;
                        case 'prolongationCost2':
                            themes.push('Пролонгация 180 дней');
                            advertiser.packetType = 'Пролонгация 180 дней';
                            advertiser.activePacket = true;
                            prolongationUpdate += finance.packet180.date * 24 * 3600 * 1000;

                            cost += finance.packet180.cost;
                            break;
                        case 'dopQuestions':
                            themes.push('Мерчандайзинг');
                            if (checkout.questions && checkout.questions.length > 0 && advertiser.dopQuestions) {
                                checkout.questions.forEach(function (question) {
                                    advertiser.dopQuestions.forEach(function (q) {
                                        if (q.id === question) {
                                            q.questionState = 'ok';
                                        }
                                    });
                                    cost += finance.dopQuestions.cost;
                                });
                            }
                            break;
                        default:
                            break;
                    }
                });
                if(prolongationUpdate !== 0){
                    addTimer(advertiser.id, 'prolongation', prolongationUpdate);
                }
                if(VipByGameUpdate !== 0) {
                    addTimer(advertiser.id, 'VipByGame', VipByGameUpdate);
                }

                themes.forEach(function (theme) {
                    mailer.sendMail({
                        theme: 'mail_template',
                        sub_theme: 'activate_options',
                        value: theme,
                        address: advertiser.email,
                        subject: 'Активация пакета услуг',
                    }, function (err) {
                        if (err) {
                            return log.log('error', 'Ошибка при отправке сообщения');
                        }
                    })
                });
                if (advertiser.referral) {
                    addPointsToReferral(advertiser, 'money', cost / 10, checkout.checkoutNumber, function (err) {
                        if(err){
                            return callback(err);
                        }
                        advertiser.save(function (err) {
                            if (err) {
                                log.log('error', 'activateOptions: ' + err);
                                return callback('Внутренняя ошибка сервера');
                            }
                            checkout.remove(function (err) {
                                if (err) {
                                    log.log('error', 'activateOptions: ' + err);
                                    return callback('Внутренняя ошибка сервера');
                                }
                                callback(null, [true])
                            });
                        });
                    });
                } else {
                    advertiser.save(function (err) {
                        if (err) {
                            log.log('error', 'activateOptions: ' + err);
                            return callback('Внутренняя ошибка сервера');
                        }
                        checkout.remove(function (err) {
                            if (err) {
                                log.log('error', 'activateOptions: ' + err);
                                return callback('Внутренняя ошибка сервера');
                            }
                            callback(null, [true])
                        });
                    });
                }
            });
        });
    },
    getAllWinners: function (administrator, callback) {
        if (administrator._type !== 'administrator') {
            return callback('Ошибка доступа');
        }
        Victory.find({}, function (err, winners) {
            if (err){
                log.log('error', 'getAllWinners: ' + err);
                return callback('Внутренняя ошибка сервера');
            }
            if(winners.length === 0){
                return callback('Победители не найдены');
            }
            let array = [];
            winners.forEach(function (winner) {
                array.push(winner.accountID, winner.userMail, winner.victoryType, winner.date.getTime());
            });
            callback(null, array);
        });
    },
    shareMoney: function (administrator, args, callback) {
        if (administrator._type !== 'administrator') {
            return callback('Ошибка доступа');
        }
        if(args.type !== 'Re' && args.type !== 'money' && args.type !== 'Pr' && args.type !== 'Lu'){
            return callback('Неверный тип');
        }
        User.find({}, function (err, users) {
            if (err){
                log.log('error', 'shareMoney: ' + err);
                return callback('Внутренняя ошибка сервера');
            }
            users.forEach(function (user) {
                addEvent(user.id, 'money' + args.type + ';' + args.value);
                mailer.sendMail({
                    theme: 'mail_template',
                    sub_theme: 'admin_gift',
                    address: user.email,
                    value: args.value + ' ' + (args.type=='money'?'Р.':args.type),
                    subject: 'Подарок от администрации'
                }, function (err) {
                    if (err) {
                        return log.log('error', 'Ошибка при отправке сообщения');
                    }
                });
            });
            callback(null, [true]);
        });
    },

    //Advertiser section
    //Options
    getOptionsCost: function (advertiser, options, callback) {
        Finance.findOne({}, function (err, finance) {
            if (err) {
                log.log('error', 'getOptionsCost: ' + err);
                return callback('Внутренняя ошибка сервера');
            }
            let array = [];
            options.forEach(function (option) {
                let optionsPrice = 0;
                switch (option) {
                    case 'prolongationCost0':
                        optionsPrice = finance.packet30.cost;
                        break;
                    case 'prolongationCost1':
                        optionsPrice = finance.packet90.cost;
                        break;
                    case 'prolongationCost2':
                        optionsPrice = finance.packet180.cost;
                        break;
                    case 'dopQuestions':
                        if(advertiser) {
                            advertiser.dopQuestions.forEach(function (question) {
                                if (question.questionState === 'wait') {
                                    optionsPrice += finance.dopQuestions.cost;
                                }
                            });
                        }
                        break;
                    case 'dopQuestion':
                        optionsPrice = finance.dopQuestions.cost;
                        break;
                    default:
                        if (finance[option]) {
                            optionsPrice = finance[option].cost;
                        }
                        break;
                }
                array.push(optionsPrice);
            });
            callback(null, array);
        });
    },
    sendPaidMail: function (user, args, callback) {
        if (user._type !== 'advertiser') {
            return callback('Ошибка доступа');
        }
        let services = [];
        let price = 0;
        Finance.findOneAndUpdate({}, {$inc: {operationNumber: 1}}, function (err, finance) {
            if (err) {
                log.log('error', 'sendPaidMail: ' + err);
                return callback('Внутренняя ошибка сервера');
            }
            let checkout = new Checkout({
                advertiserId: user.id,
                accountID: user.accountID,
                checkoutNumber: finance.operationNumber,
                createdAt: Date.now()
            });
            args.forEach(function (service) {
                switch (service) {
                    case 'prolongationCost0':
                        checkout.options.push(service);
                        price += finance.packet30.cost;
                        services.push({
                            packetValue: 1,
                            packetName: finance.packet30.name,
                            packetPrice: finance.packet30.cost
                        });
                        break;
                    case 'prolongationCost1':
                        checkout.options.push(service);
                        price += finance.packet90.cost;
                        services.push({
                            packetValue: 1,
                            packetName: finance.packet90.name,
                            packetPrice: finance.packet90.cost
                        });
                        break;
                    case 'prolongationCost2':
                        checkout.options.push(service);
                        price += finance.packet180.cost;
                        services.push({
                            packetValue: 1,
                            packetName: finance.packet180.name,
                            packetPrice: finance.packet180.cost
                        });
                        break;
                    case 'dopQuestions':
                        if (user.dopQuestions.length > 0) {
                            checkout.options.push(service);
                            user.dopQuestions.forEach(function (question) {
                                if (question.questionState === 'wait') {
                                    price += finance[service].cost;
                                    services.push({
                                        packetValue: 1,
                                        packetName: finance[service].name + ': (' + question.questionText + ', ' + question.answerText + ', ' + question.answerSite + ')',
                                        packetPrice: finance[service].cost
                                    });
                                    checkout.questions.push(question.id);
                                }
                            });
                        }
                        break;
                    default:
                        if (finance[service]) {
                            checkout.options.push(service);
                            price += finance[service].cost;
                            services.push({
                                packetValue: 1,
                                packetName: finance[service].name,
                                packetPrice: finance[service].cost
                            });
                        }
                        break;
                }
            });

            checkout.cost = price;
            checkout.save(function (err) {
                if (err) {
                    log.log('error', 'sendPaidMail: ' + err);
                    return callback('Внутренняя ошибка сервера');
                }
                mailer.sendMail({
                    theme: 'paid',
                    address: user.email,
                    user: user,
                    subject: 'Счёт на оплату',

                    operationNumber: finance.operationNumber,
                    companyName: finance.companyName,
                    services: services,
                    totalSum: price
                }, function (err) {
                    if (err) {
                        return log.log('info', 'Ошибка при отправке сообщения');
                    }
                });
                callback(null, [true]);
            });
        });
    },

    //Question
    addDopQuestion: function (advertiser, args, callback) {
        if (advertiser._type === 'advertiser') {
            if (advertiser.dopQuestions.length >= advertiser.maxDopQuestions) {
                callback('Добавлено масимальное число вопросов', [""]);
            }
            advertiser.dopQuestions.push({
                questionState: 'wait',
                questionText: args.questionText,
                answerText: args.answerText,
                answerSite: args.answerSite.search(/http/i) === -1 ? 'http://' + args.answerSite : args.answerSite
            });
            advertiser.save(function (err) {
                if (err) {
                    log.log('error', 'addDopQuestion: ' + err);
                    return callback('Внутренняя ошибка сервера');
                }
                callback(null, [advertiser.dopQuestions[advertiser.dopQuestions.length - 1].id]);
            });
        }
    },
    deleteDopQuestion: function (advertiser, args, callback) {
        if (advertiser._type === 'advertiser') {
            Checkout.find({advertiserId: advertiser.id}, function (err, checkouts) {
                if (err) {
                    log.log('error', 'deleteDopQuestion: ' + err);
                    return callback('Внутренняя ошибка сервера');
                }
                checkouts.forEach(function (checkout) {
                    checkout.questions.pull(args.id);
                    if (checkout.questions.length === 0) {
                        checkout.remove(function (err) {
                            if (err) {
                                return log.log('error', 'deleteDopQuestion: ' + err);
                            }
                        });
                    } else {
                        checkout.save(function (err) {
                            if (err) {
                                return log.log('error', 'deleteDopQuestion: ' + err);
                            }
                        });
                    }
                });

                advertiser.dopQuestions.pull(args.id);
                advertiser.save(function (err) {
                    if (err) {
                        log.log('error', 'deleteDopQuestion: ' + err);
                        return callback('Внутренняя ошибка сервера');
                    }
                    callback(null, [true]);
                });
            });
        }
    },

    logout: function (token) {
        Token.remove({tokenId: token}, function (err) {
            if (err) {
                log.log('error', 'logout: ' + err);
            }
        });
    },
    checkLogin: function (token, callback) {
        Token.findOneAndUpdate({tokenId: token}, {createdAt: Date.now()}).populate('userId').exec(function (err, tokens) {
            if (err) {
                log.log('error', 'checkLogin: ' + err);
                return callback('Внутренняя ошибка сервера');
            }
            if (!tokens) {
                return callback('Не зашёл');
            }
            let user = tokens.userId;

            if (!user) {
                return callback('Пользователь не найден');
            }
            callback(null, user);
        });
    },
    closeDB: function () {
        mongoose.disconnect();
    }
};
