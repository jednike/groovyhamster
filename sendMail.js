'use strict';
const nodemailer = require('nodemailer');
const smtpTransport = require('nodemailer-smtp-transport');
const config = require('./config.json');
const log = require('./log')(module);
const pug = require('pug');
let Email;

const transporter = nodemailer.createTransport(smtpTransport({
    host: config.email_server.host,
    tls: { rejectUnauthorized: false },
    auth: {
        user: config.email_server.username,
        pass: config.email_server.password
    }
}));
const saveEmail = function (mailOptions) {
    let email = new Email({
        html: mailOptions.html,
        address: mailOptions.to,
        subject: mailOptions.subject,
        date: Date.now()
    });
    email.save(function (err) {
        if(err){
            log.log('error', 'saveEmail: ' + err);
        }
    });
};

transporter.verify(function (error, success) {
    if (error) {
        log.log('info', error);
    } else {
        log.log('info', 'Server is ready to take our messages');
    }
});

let mailOptions = {
    from: '"GHClub" <hamster@ghclub.net>'
};

module.exports = {
    sendMail: function (mailInfo, callback) {
        switch (mailInfo.theme) {
            case 'resend':
                mailOptions.html = mailInfo.html;
                break;
            case 'paid':
                let date = new Date();
                mailOptions.html = pug.compileFile('./htmls/paid.pug')({
                    number: mailInfo.operationNumber,
                    BIK: mailInfo.user.BIK,
                    INN: mailInfo.user.INN,
                    KPP: mailInfo.user.KPP,
                    FIO: mailInfo.user.FIO,
                    corAccount: mailInfo.user.corAccount,
                    bankAccount: mailInfo.user.bankAccount,
                    accountID: mailInfo.user.accountID,
                    companyName: mailInfo.user.companyName,
                    services: mailInfo.services,
                    groovyName: mailInfo.companyName,

                    totalSum: mailInfo.totalSum,
                    stringTotalSum: require('rubles').rubles(mailInfo.totalSum),
                    date: date.getDate() + '.' + (date.getMonth() + 1) + '.' + date.getFullYear()
                });
                break;
            case 'requisites':
                mailOptions.html = pug.compileFile('./htmls/register.pug')({
                    accountID: mailInfo.user.accountID,
                    password: mailInfo.user.password
                });
                break;
            case 'invite_user':
                mailOptions.html = pug.compileFile('./htmls/invite.pug')({
                    targetText: 'Приглашение для Друга',
                    motivationText: ['Ежемесячный розыгрыш iPhone. Возможность заработать.', 'Отсутствие доната'],
                    award: mailInfo.Pr + ' Pr',
                    awardText: 'Вы на 10% ближе к выигрышу!',
                    giftText: mailInfo.accountID ? ((mailInfo.accountID === 'admin') ? 'Администрация сделала вам подарок:' : 'Аккаунт ' + mailInfo.accountID + ' сделал вам подарок:') : undefined,
                    invite: mailInfo.invite
                });
                break;
            case 'invite_advertiser':
                mailOptions.html = pug.compileFile('./htmls/invite.pug')({
                    targetText: 'Приглашение для Рекламодателя',
                    motivationText: ['Моментальное увеличение целевого трафика сайта'],
                    award: '3 дня',
                    awardText: 'оплаченного показа рекламного пакета СТАРТОВЫЙ.',
                    giftText: mailInfo.accountID ? ((mailInfo.accountID === 'admin') ? 'Администрация сделала вам подарок:' : 'Аккаунт ' + mailInfo.accountID + ' сделал вам подарок:') : undefined,
                    invite: mailInfo.invite
                });
                break;
            case 'order_money':
                mailOptions.html = pug.compileFile('./htmls/order_money.pug')({
                    referrers: mailInfo.referrers,
                    accountID: mailInfo.user.accountID,
                    endMoneyDate: mailInfo.user.endMoneyDate.getDate() + '.' + (mailInfo.user.endMoneyDate.getMonth() + 1) + '.' + mailInfo.user.endMoneyDate.getFullYear(),
                    lastPlusDate: mailInfo.user.plusMoney ? (mailInfo.user.lastPlusDate.getDate() + '.' + (mailInfo.user.lastPlusDate.getMonth() + 1) + '.' + mailInfo.user.lastPlusDate.getFullYear()) : "никогда",
                    email: mailInfo.user.email,
                    FIO: mailInfo.user.outFIO,
                    bankName: mailInfo.user.outBankName,
                    corAccount: mailInfo.user.outCorAccount,
                    bankAccount: mailInfo.user.outBankAccount,
                    BIK: mailInfo.user.outBIK,
                    INN: mailInfo.user.outINN,
                    money: mailInfo.user.money
                });
                break;
            case 'feedback':
                let args = mailInfo.args;
                mailOptions.html = pug.compileFile('./htmls/feedback.pug')(args);
                break;
            case 'mail_template':
                let variables = {};
                let compiledFunction = pug.compileFile('./htmls/mail_template.pug');

                switch (mailInfo.sub_theme) {
                    case 'activate_options':
                        variables.title = 'Пакет услуг активирован';
                        variables.value = mailInfo.value;
                        variables.subTitle = ['Поздравляем! Пакет заказанных Вами услуг', 'GH Club успешно активирован!'];
                        variables.bottom = ['ВНИМАНИЕ: Выставленные Вам счета на рекламные пакеты и доп.', 'услуги GH Club - действительны в течении 7 дней.'];
                        break;
                    case 'end_packet':
                        variables.title = 'Срок оплаченного пакета истекает';
                        variables.value = mailInfo.value;
                        variables.red = true;
                        variables.subTitle = ['Срок оплаченного Вами рекламного пакета', 'истекает через ' + (mailInfo.value === 'Тестовый' ? '3 дня' : '7 дней') + ', для продления - воспользуйтесь', 'личным кабинетом GH Club.'];
                        variables.bottom = ['ВНИМАНИЕ: Выставленные Вам счета на рекламные пакеты и доп.', 'услуги GH Club - действительны в течении 7 дней.'];
                        break;
                    case 'ended_packet':
                        variables.title = 'Срок оплаченного пакета истек';
                        variables.value = mailInfo.value;
                        variables.red = true;
                        variables.subTitle = ['Срок оплаченного Вами рекламного пакета истек,', 'для активации - воспользуйтесь', 'личным кабинетом GH Club.'];
                        variables.bottom = ['ВНИМАНИЕ: Выставленные Вам счета на рекламные пакеты и доп.', 'услуги GH Club - действительны в течении 7 дней.'];
                        break;
                    case 'end_money':
                        variables.title = 'Срок хранения на счете истекает';
                        variables.value = mailInfo.value;
                        variables.red = true;
                        variables.subTitle = ['Срок хранения денежных средств на счете GH Club', 'истекает через 7 дней, для запроса на вывод -', 'воспользуйтесь личным кабинетом GH Club.'];
                        variables.bottom = ['ВНИМАНИЕ: Согласно правилам GH Club деньги на счете хранятся', 'в течении 30 дней с момента поступления. Далее они рандомно', 'обмениваются на один из игровых ресурсов.'];
                        break;
                    case 'admin_gift':
                        variables.title = 'Подарок от администрации';
                        variables.value = mailInfo.value;
                        variables.subTitle = ['В знак признательности за лояльность нашему клубу,', 'мы делаем вам подарок.'];
                        variables.bottom = ['С Уважением Администрация GH Club.'];
                        break;
                    case 'activate_money':
                        variables.title = 'Вывод средств проведен!';
                        variables.value = mailInfo.money + ' ' + mailInfo.currency;
                        variables.subTitle = ['В случае возникновения проблем', 'свяжитесь с администрацией.'];
                        variables.bottom = ['С Уважением Администрация GH Club.'];
                        break;
                    case 'passreset':
                        variables.title = 'Восстановление пароля';
                        variables.value = mailInfo.resetCode;
                        variables.subTitle = ['Никому не сообщайте пароль восстановления.'];
                        variables.bottom = ['ВНИМАНИЕ: Номер аккаунта и пароль - являются уникальными,', 'и в случае утери, восстановлению не подлежат!', 'С Уважением Администрация GH Club.'];
                        break;
                    case 'bananswer':
                        variables.title = 'Блокировка вопроса';
                        variables.value = ['Текст вопроса: ' + mailInfo.question.questionText, 'Текст ответа: ' + mailInfo.question.answerText, 'Сайт ответа: ' + mailInfo.question.answerSite];
                        variables.subTitle = mailInfo.baseQuestion ? ['Чтобы разблокировать вопрос исправьте', 'ошибку в личном кабинете'] : ['Добавьте новый дополнительный', 'вопрос в личном кабинете'];
                        variables.bottom = ['С Уважением Администрация GH Club.'];
                        break;
                    case 'victory':
                        variables.title = 'Ваш аккаунт';
                        variables.value = mailInfo.accountID;
                        variables.subTitle = ['победил в розыгрыше приза', 'Укажите данные доставки в личном кабинете'];
                        variables.bottom = ['С Уважением Администрация GH Club.'];
                        break;
                }
                mailOptions.html = compiledFunction(variables);
                break;
        }
        mailOptions.to = mailInfo.address;
        mailOptions.subject = mailInfo.subject;

        if (mailInfo.emailID) {
            Email.remove({id: mailInfo.emailID}, function (err) {
                if (err) {
                    log.log('error', 'sendMail: ' + err);
                }
            });
        }

        let re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        if (re.test(mailInfo.address)) {
            if (mailInfo.sub_theme === 'admin_gift') {
                saveEmail(mailOptions);
                callback(null);
            } else {
                transporter.sendMail(mailOptions, function (err, info) {
                    if (err) {
                        if(!mailInfo.emailID) {
                            saveEmail(mailOptions);
                        }
                        log.log('error', 'sendMail: ' + err);
                        return callback(err);
                    }
                    callback(null);
                    log.log('info', 'Message sent: ' + info.response);
                });
            }
        } else {
            log.log('info', 'Not valid:  ' + mailInfo.address);
            callback(null);
        }
    },
    setEmail: function(email){
        Email = email;
    }
};