'use strict';
const RELEASE = true;
const express = require('express');
const jayson = require('jayson');
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
const jsonClient = jayson.client.https({
    port: 1338
});
const http = require('http');
const https = require('https');

const fs = require('fs');
const imagemagick = require('imagemagick');
const bodyParser = require('body-parser');
const app = express();
const log = require('./log')(module);

app.use(bodyParser.json({limit: '4mb'}));
app.use(bodyParser.urlencoded({limit: '4mb', extended: true}));

app.post('/upload', function (req, res) {
    if (!req.body || !req.body.width || !req.body.height || !req.body.image || !req.body.imageId) {
        res.json({success: false, error: 'Not filling fields'});
        res.end();
        return log.log('error', 'Not filling fields');
    }

    let bitmap = new Buffer(req.body.image, 'base64');
    let imageId = req.body.imageId;
    jsonClient.request('removeImage', ["", imageId], function(err, response) {
        if(err) return res.json({ok: false, error: 'Внутренняя ошибка сервера'});
        if(response.error) return res.json({ok: false, error: response.error});
        let imageName = response.result[0];
        let newPath = __dirname + '/uploads/fullsize/' + imageName;
        let thumbPath = __dirname + '/uploads/thumbs/' + imageName;
        fs.writeFile(newPath, bitmap, function (err) {
            if (err) {
                res.json({success: false, error: err});
                res.end();
                return log.log('error', 'upload: ' + err);
            }
            fs.writeFile(thumbPath, bitmap, function (err) {
                if (err) {
                    res.json({success: false, error: err});
                    res.end();
                    return log.log('error', 'upload: ' + err);
                }
                if (req.body.width > req.body.height) {
                    imagemagick.resize({
                        srcPath: newPath,
                        dstPath: newPath,
                        width: 500
                    }, function (err) {
                        if (err) {
                            log.log('error', 'upload: ' + err);
                            res.json({success: false, error: err});
                            return res.end();
                        }
                        imagemagick.resize({
                            srcPath: newPath,
                            dstPath: thumbPath,
                            width: 150
                        }, function (err) {
                            if (err) {
                                log.log('error', 'upload: ' + err);
                                res.json({success: false, error: err});
                                return res.end();
                            }
                            res.json({success: true});
                            res.end();
                        });
                    });
                } else {
                    imagemagick.resize({
                        srcPath: newPath,
                        dstPath: newPath,
                        height: 500
                    }, function (err) {
                        if (err) {
                            log.log('error', 'upload: ' + err);
                            res.json({success: false, error: err});
                            return res.end();
                        }
                        imagemagick.resize({
                            srcPath: newPath,
                            dstPath: thumbPath,
                            height: 150
                        }, function (err) {
                            if (err) {
                                log.log('error', 'upload: ' + err);
                                res.json({success: false, error: err});
                                return res.end();
                            }
                            res.json({success: true});
                            res.end();
                        });
                    });
                }
            });
        });
    });
});
app.get('/uploads/fullsize/:file', function (req, res) {
    let file = req.params.file;
    if(!fs.existsSync(__dirname + '/uploads/fullsize/' + file)){
        return res.end();
    }
    let img = fs.readFileSync(__dirname + '/uploads/fullsize/' + file);
    res.writeHead(200, {'Content-Type': 'image/jpg'});
    res.end(img, 'binary');
});
app.get('/uploads/thumbs/:file', function (req, res) {
    let file = req.params.file;
    if(!fs.existsSync(__dirname + '/uploads/thumbs/' + file)){
        return res.end();
    }
    let img = fs.readFileSync(__dirname + '/uploads/thumbs/' + file);
    res.writeHead(200, {'Content-Type': 'image/jpg'});
    res.end(img, 'binary');
});

if(!RELEASE){
    const mongoose = require('mongoose');
    const options = {promiseLibrary: require('bluebird')};
    mongoose.connect(require('./config.json').mongoose.uri, options);
    mongoose.connection.on('open', function () {
        log.log('info', 'Connected to database!');
    });
    const Lottery = require('./schemas/lottery')(mongoose);
    let User;
    require('./schemas/user')(mongoose, function (user) {
        User = user;
    });

    app.get('/quests', function (req, res) {
        Lottery.find({}, function (err, lotteries) {
            if (err) {
                return res.json(err);
            }

            let formCheckout = "<!DOCTYPE HTML><html><body>";
            lotteries.forEach(function (lottery) {
                formCheckout += "<form method='post' action='/questUpdate'>" +
                    "<input type='hidden' name='id' value=" + lottery.id + ">" + lottery.type + "/" + lottery.state +
                    "<br/><input name='date' value='" + lottery.date + "'>" +
                    "<input type='submit' /></form>";
            });

            formCheckout += "</body></html>";
            res.end(formCheckout);
        });
    });
    app.post('/questUpdate', function (req, res) {
        Lottery.findById(req.body.id, function (err, lottery) {
            if(err){
                return res.json(err);
            }
            if(lottery){
                lottery.date = req.body.date;
                lottery.save(function (err) {
                    if(err){
                        return res.json(err);
                    }
                    res.redirect('/quests');
                });
            }
        });
    });

    // app.get('/variables', function (req, res) {
    //     res.writeHead(200, {'Content-Type': 'text/html'});
    //     let form = "<!DOCTYPE HTML><html><body>" +
    //         "<form method='post' action='/getVariables'>" +
    //         "<input name='userToken' value='" + userToken + "'>" +
    //         "<input type='submit' /></form>" +
    //         "</body></html>";
    //     res.end(form);
    // });
    // app.post('/getVariables', function (req, res) {
    //     userToken = req.body.userToken;
    //     Token.findOne({tokenId: userToken}, function (err, tokens) {
    //         if (err) {
    //             return res.json(err);
    //         }
    //         if (!tokens) {
    //             return res.json(err);
    //         }
    //         User.findById(tokens.userId, function (err, user) {
    //             if (err) {
    //                 return res.json(err);
    //             }
    //
    //             if (user) {
    //                 let formCheckout = "<!DOCTYPE HTML><html><body>";
    //                 let variables = ['accountID', 'activeAnimal', 'animalNumber', 'lotteryNumber', 'bioTimer' ,'Re', 'Pr', 'Lu', 'Al',
    //                     'Cf', 'star', 'maxStar', 'money'];
    //                 variables.forEach(function (object) {
    //                     formCheckout += "<form method='post' action='/updateVariable'>" +
    //                         "<input name='key' value=" + object + ">" +
    //                         "<input name='value' value='" + user[object] + "'>" +
    //                         "<input type='submit' /></form>";
    //                 });
    //
    //                 formCheckout += "</body></html>";
    //                 res.end(formCheckout);
    //             }
    //         });
    //     });
    // });
    // app.post('/updateVariable', function (req, res) {
    //     Token.findOne({tokenId: userToken}, function (err, tokens) {
    //         if (err) {
    //             return res.json(err);
    //         }
    //         if (!tokens) {
    //             return res.json(err);
    //         }
    //         User.findById(tokens.userId, function (err, user) {
    //             if (err) {
    //                 return res.json(err);
    //             }
    //             if(user){
    //                 user[req.body.key] = req.body.value;
    //                 user.save(function (err) {
    //                     if(err){
    //                         return res.json(err);
    //                     }
    //                     res.redirect('/variables');
    //                 });
    //             }
    //         });
    //     });
    // });
    //
    // app.get('/checkouts', function (req, res) {
    //     res.writeHead(200, {'Content-Type': 'text/html'});
    //     let form = "<!DOCTYPE HTML><html><body>" +
    //         "<form method='post' action='/getCheckouts'>" +
    //         "<input name='adminToken' value='adminToken'>" +
    //         "<input type='submit' /></form>" +
    //         "</body></html>";
    //     res.end(form);
    // });
    // app.post('/getCheckouts', function (req, res) {
    //     adminToken = req.body.adminToken;
    //     jsonClient.request('getAllCheckouts', [adminToken], function(err, response) {
    //         if(err) {
    //             res.json(err);
    //             return log.log('error', 'getCheckouts: ' + err);
    //         }
    //         let formCheckout = "<!DOCTYPE HTML><html><body>";
    //         if(response.result && response.result.length > 0) {
    //             for (let i = 0; i < response.result.length; i++) {
    //                 formCheckout += "<form method='post' action='/activateCheckout'>" +
    //                     "<input name='checkoutNumber' value=" + response.result[i] + ">" +
    //                     "<input name='adminToken' value='" + adminToken + "'>" +
    //                     "<input type='submit' /></form>";
    //                 i++;
    //             }
    //         }
    //         formCheckout += "</body></html>";
    //         res.end(formCheckout);
    //     });
    // });
    // app.post('/activateCheckout', function (req, res) {
    //     jsonClient.request('activateOptions', [adminToken, req.body.checkoutNumber], function(err, response) {
    //         if(err) return log.log('error', 'getCheckouts: ' + err);
    //         res.json(response.result);
    //     });
    // });
}

app.post('/sendInvite', function (req, res) {
    if (!req.body.email) {
        return res.json({ok: false, error: 'Не указан email'});
    } else if (!req.body.type || req.body.type === 'administrator') {
        return res.json({ok: false, error: 'Не указан тип пользователя'});
    }
    jsonClient.request('sendInvite', ["", req.body.type, req.body.email, '', 'verySecretKeyForAdminGift'], function (err, response) {
        if (err) return res.json({ok: false, error: 'Внутренняя ошибка сервера'});
        if (response.error) {
            return res.json({ok: false, error: response.error});
        }
        return res.json({ok: true});
    });
});

http.createServer(app).listen(8080, function () {
    log.log('info', 'Server http listen on 8080');
});
const credentials = {key: fs.readFileSync('keys/server.key', 'utf8'), cert: fs.readFileSync('keys/server.crt', 'utf8')};
https.createServer(credentials, app).listen(8443, function () {
    log.log('info', 'Server https listen on 8443');
});