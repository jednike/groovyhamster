'use strict';
module.exports = function(mongoose){
    const VictorySchema = new mongoose.Schema({
        accountID: {type: String, required: true},
        userMail: {type: String, required: true},
        victoryType: {type: String, required: true},
        verifyUser: {type: Boolean, default: false},
        date: {type: Date, required: true, default: Date.now()}
    });
    return mongoose.model('victory', VictorySchema);
};