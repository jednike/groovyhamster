'use strict';
module.exports = function(mongoose){
    const MoneyRequestSchema = new mongoose.Schema({
        userId: {type: mongoose.Schema.Types.ObjectId, ref: 'user'},
        accountID: {type: String, required: true},
        money: {type: Number, required: false, default: 0},
        date: {type: Date, required: true, default: Date.now()},
        createdAt: {type: Date, expires: '8d', default: Date.now()}
    });
    return mongoose.model('money_request', MoneyRequestSchema);
};