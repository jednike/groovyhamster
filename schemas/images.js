'use strict';
module.exports = function(mongoose){
    const ImagesSchema = new mongoose.Schema({
        accountID: {type: String, default: ""},
        date: {type: Date, required: true, default: Date.now()},
        createdAt: {type: Date, expires: '1d', default: Date.now()}
    });
    return mongoose.model('Images', ImagesSchema);
};