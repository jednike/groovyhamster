'use strict';
module.exports = function(mongoose){
    const FinancesSchema = new mongoose.Schema({
        operationNumber: {type: Number, default: 1},
        exchangeLuck: {
            Cf: {
                cost: {type: Number, default: 100},
                award: {type: Number, default: 3}
            },
            Re: {
                cost: {type: Number, default: 999},
                award: {type: Number, default: 1}
            },
            squirrel: {
                cost: {type: Number, default: 250}
            }
        },
        startPacket: {
            name: {type: String, default: 'Регистрация аккаунта с пакетом услуг на 30 дней'},
            date: {type: Number, default: 30},
            cost: {type: Number, default: 5000}
        },
        packet30: {
            name: {type: String, default: 'Пакет на 30 дней'},
            date: {type: Number, default: 30},
            cost: {type: Number, default: 5000}
        },
        packet90: {
            name: {type: String, default: 'Пакет на 90 дней'},
            date: {type: Number, default: 90},
            cost: {type: Number, default: 10000}
        },
        packet180: {
            name: {type: String, default: 'Пакет на 180 дней'},
            date: {type: Number, default: 180},
            cost: {type: Number, default: 15000}
        },
        VipByGame: {
            name: {type: String, default: 'Показывать в игре'},
            date: {type: Number, default: 30},
            cost: {type: Number, default: 10000}
        },
        VipByCountry: {
            name: {type: String, default: 'Показывать по всей стране'},
            date: {type: Number, default: 30},
            cost: {type: Number, default: 35000}
        },
        dopQuestions: {
            name: {type: String, default: 'Дополнительный вопрос'},
            cost: {type: Number, default: 500}
        },
        afterParty: {
            give: {
                cost: {type: Number, default: 10},
                award: {type: Number, default: 1}
            },
            pickUp: {
                cost: {type: Number, default: 10},
                award: {type: Number, default: 10}
            }
        },
        adAward: {
            cost: {type: Number, default: 0.1}
        },
        lottery: {
            text: {type: String, default: "Это 7."},
            url: {type: String, default: "https://www.apple.com/ru/iphone-7/"},
            currentText: {type: String, default: "Это 7."},
            currentUrl: {type: String, default: "https://www.apple.com/ru/iphone-7/"}
        },
        adSingleAward: {
            cost: {type: Number, default: 0.5}
        },
        referrerAward: {
            cost: {type: Number, default: 100}
        },
        companyName: {type: String, default: 'ООО Грувихамстер'},
        lastQuestNumber: {type: Number, default: 0},
        currentQuest: {
            targetType: {type: String, default: 'Re'},
            targetValue: {type: Number, default: 30},
            awardType: {type: String, default: 'Lu'},
            awardValue: {type: Number, default: 50},
            date: {type: Date, default: Date.now()}
        }
    });
    return mongoose.model('Finance', FinancesSchema);
};