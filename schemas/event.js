'use strict';
module.exports = function(mongoose) {
    const EventSchema = new mongoose.Schema({
        userId: {type: mongoose.Schema.Types.ObjectId, ref: 'user'},
        type: {type: String, required: true},
        read: {type: Boolean, default: false},
        date: {type: Date, required: true, default: Date.now()}
    });

    return mongoose.model('Event', EventSchema);
};