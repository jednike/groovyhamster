'use strict';
module.exports = function(mongoose) {
    const EmailSchema = new mongoose.Schema({
        html: {type: String, required: true},
        address: {type: String, required: true},
        subject: {type: String, required: true},
        date: {type: Date, required: true, default: Date.now()}
    });

    return mongoose.model('Email', EmailSchema);
};