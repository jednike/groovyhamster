'use strict';
const bcrypt = require('bcrypt'),
    uniqueValidator = require('mongoose-unique-validator'),
    extend = require('mongoose-schema-extend');

module.exports = function(mongoose, UserSchema) {
    const AdvertiserSchema = UserSchema.extend({
        // Company info
        companyName: {type: String, required: true, default: ''},
        contactNum: {type: String, required: false, default: ''},
        sphereOfActivity: {type: String, uppercase: true, required: true, default: ''},
        activity: {type: String, required: false, default: ''},
        delivery: {type: String, required: false, default: ''},
        slogan: {type: String, required: false, default: ''},
        site: {type: String, required: false, default: ''},

        // My wallet
        FIO: {type: String, required: false, default: ''},
        bankName: {type: String, required: false, default: ''},
        INN: {type: String, required: false, default: ''},
        KPP: {type: String, required: false, default: ''},
        BIK: {type: String, required: false, default: ''},
        bankAccount: {type: String, required: false, default: ''},
        corAccount: {type: String, required: false, default: ''},

        // Prolongation info
        activePacket: {type: Boolean, required: false, default: false},
        packetType: {type: String, required: false, default: 'Стартовый'},

        VipText: {type: String, required: false, default: ""},
        VipType: {type: String, required: false, default: "VipByGame"},
        VipByGameState: {type: Boolean, required: false, default: false},
        VipByGameAward: {
            type: {type: String, required: false},
            value: {type: Number, required: false}
        },
        VipByCountryState: {type: Boolean, required: false, default: false},

        tenStars: {type: Boolean, required: false},

        // Answers statistic
        totalAnswers: {type: Number, required: false, default: 0},
        falseAnswers: {type: Number, required: false, default: 0},

        // Merchandise info
        questionState: {type: String, required: true, default: 'ok'},
        questionText: {type: String, required: true, default: ''},
        answerText: {type: Number, required: true, default: 0},
        answerSite: {type: String, required: true, default: ''},

        maxDopQuestions: {type: Number, required: false, default: 4},
        dopQuestions: [{
            totalAnswers: {type: Number, required: false, default: 0},
            falseAnswers: {type: Number, required: false, default: 0},
            questionState: {type: String, required: false, default: 'wait'},
            questionText: {type: String, required: false},
            answerText: {type: Number, required: false},
            answerSite: {type: String, required: false}
        }]
    });

    AdvertiserSchema.path('questionText', {
        set: function(questionText) {
            this.questionState = 'ok';
            return questionText;
        }
    });
    AdvertiserSchema.path('answerSite', {
        set: function(answerSite) {
            if(answerSite.search(/http/i) === -1) {
                return 'http://' + answerSite;
            }
            return answerSite;
        }
    });
    AdvertiserSchema.path('site', {
        set: function(site) {
            if(site.search(/http/i) === -1) {
                return 'http://' + site;
            }
            return site;
        }
    });

    AdvertiserSchema.method('verifyPassword', function (password, callback) {
        bcrypt.compare(password, this.hash, callback);
    });
    AdvertiserSchema.plugin(uniqueValidator);

    return mongoose.model('advertiser', AdvertiserSchema);
};