'use strict';
module.exports = function(mongoose){
    const InviteSchema = new mongoose.Schema({
        userId: {type: mongoose.Schema.Types.ObjectId, ref: 'user'},
        accountID: {type: String, required: true},
        invite: {type: String, required: true},
        email: {type: String, required: true},
        type: {type: String, required: true},
        createdAt: {type: Date, expires: '7d', default: Date.now()}
    });

    return mongoose.model('Invite', InviteSchema);
};