'use strict';
const bcrypt = require('bcrypt'),
    uniqueValidator = require('mongoose-unique-validator');

module.exports = function(mongoose, callback) {
    const UserSchema = new mongoose.Schema({
        accountID: {type: String, required: true, unique: true},
        invite: {type: String, required: true},

        activeAnimal: {type: String, required: true, default: "hamster"},
        animalNumber: {type: Number, required: true, default: 0},
        squirrelNumber: {type: Number, required: true, default: 0},
        hamsterNumber: {type: Number, required: true, default: 0},
        Re: {type: Number, required: true, default: 0},
        Pr: {type: Number, required: true, default: 0},
        Lu: {type: Number, required: true, default: 0},
        Al: {type: Number, required: true, default: 0},
        Cf: {type: Number, required: true, default: 12},
        star: {type: Number, required: true, default: 0},
        maxStar: {type: Number, required: true, default: 0},

        dayQuest: {
            value: {type: Number, required: false, default: 0},
            dayRe: {type: Number, required: false, default: 0},
            dayPr: {type: Number, required: false, default: 0},
            state: {type: Boolean, required: false, default: false},
            dayAnswers: {type: Number, required: false, default: 0}
        },

        nextGame: {
            needMinus: {type: Boolean, required: false, default: false},
            chance: {type: Number, required: false, default: 0},
            type: {type: String, required: false, default: ''}
        },

        // User info
        referral: {type: String, required: false},
        resetCode: [{type: String, required: false}],
        lastAwardID: {type: String, required: false},
        awards: [
            {
                companyId: {type: mongoose.Schema.Types.ObjectId, ref: 'user'},
                questionNumber:  {type: Number, required: false, default: 0},
                currentGt: {type: Number, required: false, default: 0}
            }],
        favorites: [{companyId: {type: mongoose.Schema.Types.ObjectId, ref: 'user'}}],
        referrers: {
            advertiser: [{
                companyId: {type: mongoose.Schema.Types.ObjectId, ref: 'user'},
                date: {type: Date, required: false, default: Date.now()},
                checks: [{type: Number, default: 0}],
                cost: {type: Number, required: false}
            }],
            user: [{type: mongoose.Schema.Types.ObjectId, ref: 'user'}]
        },

        // Gallery
        freebie: {type: Boolean, required: false, default: false},
        imagePath: {type: String, required: false, default: ""},
        motivationText: {type: String, required: false, default: ""},
        motivationTextDate: {type: Date, required: false, default: Date.now()},

        numberOfViews: {type: Number, required: false, default: 0},
        numberOfLikes: {type: Number, required: false, default: 0},
        numberOfDislikes: {type: Number, required: false, default: 0},
        likeList: [{
            accountID: {type: String},
            like: {type: Boolean, required: false, default: false},
            phanks: {type: Boolean, required: false, default: false}
        }],

        country: {type: String, uppercase: true, required: true, default: ''},
        city: {type: String, uppercase: true, required: true, default: ''},
        email: {type: String, required: true, default: ''},

        // My wallet
        outFIO: {type: String, required: false, default: ''},
        outBankName: {type: String, required: false, default: ''},
        outINN: {type: String, required: false, default: ''},
        outKPP: {type: String, required: false, default: ''},
        outBIK: {type: String, required: false, default: ''},
        outBankAccount: {type: String, required: false, default: ''},
        outCorAccount: {type: String, required: false, default: ''},

        superType: {type: Boolean, required: false, default: false},

        plusMoney: {type: Boolean, required: false, default: false},
        lastPlusDate: {type: Date, required: false, default: Date.now()},
        endMoneyDate: {type: Date, required: false, default: Date.now()},
        activeMoney: {type: Boolean, required: false, default: false},
        money: {type: Number, required: false, default: 0},

        lotteryState: {type: String, required: false, default: 'none'},
        lotteryNumber: {type: Number, required: false, default: 0},
        lotteryPoints: [{type: Number, required: false, default: 0}],
        lotteryDate: {type: Date, required: false, default: Date.now()},

        questionsState: {
            advertisers: [
                {
                    userId: {type: mongoose.Schema.Types.ObjectId, ref: 'user'},
                    questionNumber: {type: Number, required: false, default: 0}
                }],
            question1: {type: Number, required: false, default: 0},
            question5: {type: Number, required: false, default: 0},
            question15: {type: Number, required: false, default: 0}
        },

        // Session
        maxAnswerPerDay: {type: Number, required: false, default: 5},

        registerDate: {type: Date, required: false, default: Date.now()},

        // Security info
        pass: {type: String, required: true},
        salt: {type: String, required: true},
        hash: {type: String, required: true}
    }, { collection : 'users', discriminatorKey : '_type'});

    UserSchema
        .virtual('password')
        .get(function () {
            return this.pass;
        })
        .set(function (password) {
            this._password = password;
            this.pass = password;
            let salt = this.salt = bcrypt.genSaltSync(10);
            this.hash = bcrypt.hashSync(password, salt);
        });
    UserSchema.method('verifyPassword', function (password, callback) {
        bcrypt.compare(password, this.hash, callback);
    });
    UserSchema.static('authenticate', function (accountID, password, callback) {
        this.findOne({accountID: accountID}, function (err, user) {
            if (err) {
                return callback(err);
            }

            if (!user) {
                return callback(null, false);
            }

            user.verifyPassword(password, function (err, passwordCorrect) {
                if (err) {
                    return callback(err);
                }

                if (!passwordCorrect) {
                    return callback(null, false);
                } else {
                    return callback(null, user);
                }
            });
        });
    });
    UserSchema.pre('save', function (next) {
        this.Re = this.Re > 1000 ? 1000: this.Re < 0? 0: this.Re;
        this.Pr = this.Pr > 1000 ? 1000: this.Pr < 0? 0: this.Pr;
        this.Lu = this.Lu > 1000 ? 1000: this.Lu < 0? 0: this.Lu;
        this.Al = this.Al > 100 ? 100: this.Al < 0? 0: this.Al;
        this.Cf = this.Cf > 12 ? 12: this.Cf < 0? 0: this.Cf;
        this.lotteryNumber = this.lotteryNumber > 3 ? 3: this.lotteryNumber < 0? 0: this.lotteryNumber;
        next();
    });
    UserSchema.plugin(uniqueValidator);

    callback(mongoose.model('user', UserSchema), UserSchema);
};