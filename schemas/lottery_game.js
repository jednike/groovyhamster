'use strict';
module.exports = function(mongoose) {
    const LotteryGameSchema = new mongoose.Schema({
        userId: {type: mongoose.Schema.Types.ObjectId, ref: 'user'},
        accountID: {type: String, default: ""},
        points: {type: Number, default: 0},
        lotteryType: {type: String, default: ""}
    });
    LotteryGameSchema.static('checkOnWin', function (lotteryType, points, callback) {
        this.find({lotteryType: lotteryType}).sort({'points': -1}).limit(1).exec(function (err, players) {
            if(players[0] && players[0].points === points){
                return callback(false);
            }
            return callback(true);
        });
    });
    return mongoose.model('LotteryGame', LotteryGameSchema);
};