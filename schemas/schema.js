'use strict';
const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');

require('./user')(mongoose, function (user, UserSchema) {
    module.exports.user = user;
    module.exports.advertiser = require('./advertiser')(mongoose, UserSchema);
    module.exports.administrator = require('./administrator')(mongoose, UserSchema);
});
module.exports.timer = require('./timers')(mongoose);
module.exports.lottery = require('./lottery')(mongoose);
module.exports.event = require('./event')(mongoose);
module.exports.answer = require('./answer')(mongoose);
module.exports.invite = require('./invite')(mongoose);
module.exports.checkout = require('./checkout')(mongoose);
module.exports.money_request = require('./money_request')(mongoose);
module.exports.finances = require('./finance')(mongoose);
module.exports.tokens = require('./token')(mongoose);
module.exports.victory = require('./victory')(mongoose);
module.exports.image = require('./images')(mongoose);
module.exports.lottery_game = require('./lottery_game')(mongoose);
module.exports.email = require('./email')(mongoose);