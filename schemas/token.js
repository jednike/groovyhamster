'use strict';
module.exports = function(mongoose) {
    const TokenSchema = new mongoose.Schema({
        userId: {type: mongoose.Schema.Types.ObjectId, ref: 'user'},
        tokenId: {type: String, required: true},
        userType:  {type: String, default: 'user'},
        createdAt: {type: Date, expires: '2d', default: Date.now()}
    });
    return mongoose.model('Token', TokenSchema);
};