'use strict';
module.exports = function(mongoose){
    const AnswerSchema = new mongoose.Schema({
        userId: {type: mongoose.Schema.Types.ObjectId, ref: 'user'},
        advertiserId: {type: mongoose.Schema.Types.ObjectId, ref: 'user'},
        answerText: {type: String, required: false},
        questionType: {type: String, required: false},
        questionNumber: {type: Number, required: false, default: 0},
        answered: {type: Boolean, required: true, default: false},
        type: {type: Boolean, required: false},
        createdAt: { type: Date, expires: '1d', default: Date.now() }
    });
    return mongoose.model('Answer', AnswerSchema);
};