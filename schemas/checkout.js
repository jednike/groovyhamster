'use strict';
module.exports = function(mongoose){
    const CheckoutSchema = new mongoose.Schema({
        advertiserId: {type: mongoose.Schema.Types.ObjectId, ref: 'user'},
        accountID: {type: String, default: ''},
        checkoutNumber: {type: Number, required: true},
        options: [{type: String, required: false}],
        questions: [{type: String, required: false}],
        cost: {type: Number, required: false, default: 0},
        date: {type: Date, required: true, default: Date.now()},
        createdAt: {type: Date, expires: '14d', default: Date.now()}
    });
    return mongoose.model('checkout', CheckoutSchema);
};