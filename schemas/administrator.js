'use strict';
const bcrypt = require('bcrypt'),
    uniqueValidator = require('mongoose-unique-validator'),
    extend = require('mongoose-schema-extend');

module.exports = function(mongoose, UserSchema) {
    const AdministratorSchema = UserSchema.extend({

    });
    AdministratorSchema.method('verifyPassword', function (password, callback) {
        bcrypt.compare(password, this.hash, callback);
    });
    AdministratorSchema.plugin(uniqueValidator);

    return mongoose.model('administrator', AdministratorSchema);
};