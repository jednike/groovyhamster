'use strict';
module.exports = function(mongoose) {
    const TimerSchema = new mongoose.Schema({
        userId: {type: mongoose.Schema.Types.ObjectId, ref: 'user'},
        notification: {type: Boolean, default: true},
        type: {type: String, required: true},
        date: {type: Date, default: Date.now()}
    });
    return mongoose.model('Timer', TimerSchema);
};