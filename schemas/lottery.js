'use strict';
module.exports = function(mongoose) {
    const LotterySchema = new mongoose.Schema({
        type: {type: String, required: true},
        state: {type: String, required: false, default: 'waiting'},
        text: {type: String, required: false, default: ''},
        url: {type: String, required: false, default: ''},
        date: {type: Date, required: true, default: Date.now()}
    });
    return mongoose.model('Lottery', LotterySchema);
};