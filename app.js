// HTTPS
'use strict';
const VERSION_ID = 13;
const fs = require('fs');
const jayson = require('jayson');
const mailer = require('./sendMail');
const database = require('./data');
const config = require('./config.json');
const log = require('./log')(module);

database.startup(config.mongoose.uri, mailer);

const server = jayson.server({
    login: function (args, callback) {
        let result = checkArgsLength(args, 3, ['string', 'string', 'string']);
        if (result !== 'OK') {

            return callback({code: 500, message: result}, [""]);
        }
        database.checkLogin(args[0], function (err, user) {
            if (user) {
                callback({code: 500, message: 'Уже авторизован'}, [""]);
            } else {
                database.authenticate(args[1], args[2], function (err, result) {
                    if (err) {
                        return callback({code: 500, message: err}, [""]);
                    }
                    if (!result) {
                        callback({code: 404, message: "Пользователь не найден"}, [""]);
                    } else {
                        callback(null, result);
                    }
                });
            }
        });
    },
    register: function (args, callback) {
        let result = checkArgsLength(args, 1, ['string']);
        if (result !== 'OK') {
            return callback({code: 500, message: result}, [false]);
        }
        database.checkLogin(args[0], function (err, user) {
            if (user) {
                callback({code: 500, message: 'Уже авторизован'}, [""]);
            } else {
                let objectArgs;
                if (args[1] === 'user' || args[1] === 'administrator') {
                    let result = checkArgsLength(args, 8, ['string', 'string', 'string', 'string', 'string', 'string', 'string', 'string']);
                    if (result !== 'OK') {
                        return callback({code: 500, message: result}, [false]);
                    }
                    objectArgs = {
                        type: args[1],
                        accountID: args[2],
                        invite: args[3],
                        country: args[4],
                        city: args[5],
                        email: args[6],
                        password: args[7]
                    };
                } else {
                    let result = checkArgsLength(args, 20, ['string', 'string', 'string', 'string', 'string', 'string', 'string', 'string',
                        'string', 'string', 'string', 'string', 'string', 'string', 'string', 'string',
                        'string', 'string', 'string', 'string', 'string']);
                    if (result !== 'OK') {
                        return callback({code: 500, message: result}, [false]);
                    }
                    objectArgs = {
                        type: args[1],
                        accountID: args[2],
                        invite: args[3],
                        country: args[4],
                        city: args[5],
                        email: args[6],
                        password: args[7],

                        companyName: args[8],
                        sphereOfActivity: args[9],
                        FIO: args[10],

                        questionText: args[11],
                        answerText: args[12],
                        answerSite: args[13],

                        bankName: args[14],
                        INN: args[15],
                        KPP: args[16],
                        BIK: args[17],
                        bankAccount: args[18],
                        corAccount: args[19]
                    };
                }
                if (objectArgs) {
                    database.saveUser(objectArgs, function (err, newUser) {
                        if (err) {
                            return callback({code: 500, message: err}, [false]);
                        } else {
                            if (newUser) {
                                callback(null, [true]);
                            } else {
                                callback(null, [false]);
                            }
                        }
                    })
                }
            }
        });
    },
    checkInvite: function (args, callback) {
        let result = checkArgsLength(args, 2, ['string', 'string']);
        if (result !== 'OK') {
            return callback({code: 500, message: result}, [false]);
        }

        database.checkLogin(args[0], function (err, user) {
            if (user) {
                return callback({code: 500, message: 'Уже авторизован'}, [false]);
            }
            database.checkInvite({
                invite: args[1]
            }, function (err, accountInfo) {
                if (err) {
                    return callback({code: 500, message: err}, [false]);
                }

                callback(null, [true, accountInfo.accountID, accountInfo.type, accountInfo.email]);
            });
        });
    },
    logout: function (args, callback) {
        let result = checkArgsLength(args, 1, ['string']);
        if (result !== 'OK') {
            return callback({code: 500, message: result}, [false]);
        }
        database.checkLogin(args[0], function (err, user) {
            if (err) {
                return callback({code: 500, message: err}, [false]);
            }
            if (!user) {
                return callback({code: 500, message: 'Не авторизован'}, [false]);
            }

            database.logout(args[0]);
            callback(null, [true]);
        });
    },
    sendInvite: function (args, callback) {
        let result = checkArgsLength(args, 3, ['string', 'string', 'string']);
        if (result !== 'OK') {
            return callback({code: 500, message: result}, [false]);
        }
        database.checkLogin(args[0], function (err, user) {
            database.createInvite(user, args, function (err, result) {
                if (err) {
                    return callback({code: 500, message: err}, [false]);
                }
                // if (args[2] === "secretKey") {
                //     return callback(null, [result.invite]);
                // }
                mailer.sendMail({
                    theme: 'invite_' + args[1],
                    address: args[2],
                    Pr: result.Pr,
                    invite: result.invite,
                    accountID: result.accountID,
                    subject: 'Приглашение в клуб'
                }, function (err) {
                    if (err) {
                        return callback({code: 500, message: 'Ошибка при отправке сообщения'}, [false]);
                    }
                    callback(null, [true]);
                });
            });
        });
    },

    sendResetCode: function (args, callback) {
        let result = checkArgsLength(args, 2, ['string', 'string']);
        if (result !== 'OK') {
            return callback({code: 500, message: result}, [false]);
        }
        database.sendResetCode(args[1], function (err, result) {
            if (err) {
                return callback({code: 500, message: err}, [false]);
            }
            callback(null, [true]);
        });
    },
    resetPassword: function (args, callback) {
        let result = checkArgsLength(args, 2, ['string', 'string']);
        if (result !== 'OK') {
            return callback({code: 500, message: result}, [""]);
        }
        database.resetPassword(args[1], function (err, result) {
            if (err) {
                return callback({code: 500, message: err}, [""]);
            }
            callback(null, result);
        });
    },

    addDopQuestion: function (args, callback) {
        let result = checkArgsLength(args, 4, ['string', 'string', 'number', 'string']);
        if (result !== 'OK') {
            return callback({code: 500, message: result}, [false]);
        }
        database.checkLogin(args[0], function (err, user) {
            if (err) {
                return callback({code: 500, message: err}, [false]);
            }
            database.addDopQuestion(user, {
                questionText: args[1],
                answerText: args[2],
                answerSite: args[3]
            }, function (err, result) {
                if (err) {
                    return callback({code: 500, message: err}, [false]);
                } else {
                    callback(null, result);
                }
            });
        });
    },
    deleteDopQuestion: function (args, callback) {
        let result = checkArgsLength(args, 2, ['string', 'string']);
        if (result !== 'OK') {
            return callback({code: 500, message: result}, [false]);
        }
        database.checkLogin(args[0], function (err, user) {
            if (err) {
                return callback({code: 500, message: err}, [false]);
            }
            database.deleteDopQuestion(user, {
                id: args[1]
            }, function (err, result) {
                if (err) {
                    return callback({code: 500, message: err}, [false]);
                }
                callback(null, result);
            });
        });
    },

    addToFavorites: function (args, callback) {
        let result = checkArgsLength(args, 2, ['string', 'string']);
        if (result !== 'OK') {
            return callback({code: 500, message: result}, [false]);
        }
        database.checkLogin(args[0], function (err, user) {
            if (err) {
                return callback({code: 500, message: err}, [false]);
            }

            database.addToFavorites(user, {answerId: args[1]}, function (err, result) {
                if (err) {
                    return callback({code: 500, message: err}, [false]);
                }
                callback(null, result);
            })
        });
    },
    removeFavorite: function (args, callback) {
        let result = checkArgsLength(args, 2, ['string', 'string']);
        if (result !== 'OK') {

            return callback({code: 500, message: result}, [false]);
        }
        database.checkLogin(args[0], function (err, user) {
            if (err) {
                return callback({code: 500, message: err}, [false]);
            }

            database.removeFavorite(user, {id: args[1]}, function (err, result) {
                if (err) {
                    return callback({code: 500, message: err}, [false]);
                }
                callback(null, result);
            })
        });
    },
    getQuestion: function (args, callback) {
        let result = checkArgsLength(args, 1, ['string']);
        if (result !== 'OK') {

            return callback({code: 500, message: result}, [""]);
        }
        database.checkLogin(args[0], function (err, user) {
            if (err) {
                return callback({code: 500, message: err}, [""]);
            }

            database.getQuestion(user, {type: args[1], advertiserID: args[2]}, function (err, result) {
                if (err) {
                    return callback({code: 500, message: err}, [""]);
                }
                callback(null, result);
            });
        });
    },
    checkAnswer: function (args, callback) {
        let result = checkArgsLength(args, 3, ['string', 'string', 'string']);
        if (result !== 'OK') {

            return callback({code: 500, message: result}, [false]);
        }
        database.checkLogin(args[0], function (err, user) {
            if (err) {
                return callback({code: 500, message: err}, [false]);
            }

            database.checkAnswer(user, {answerId: args[1], answerText: args[2]}, function (err, result) {
                if (err) {
                    return callback({code: 500, message: err}, [false]);
                }
                callback(null, result);
            });
        });
    },

    getAdInGame: function (args, callback) {
        let result = checkArgsLength(args, 1, ['string']);
        if (result !== 'OK') {

            return callback({code: 500, message: result}, [false]);
        }
        database.checkLogin(args[0], function (err, user) {
            if (err) {
                return callback({code: 500, message: err}, [false]);
            }

            database.getAdInGame(user, function (err, result) {
                if (err) {
                    return callback({code: 500, message: err}, [false]);
                }
                callback(null, result);
            })
        });
    },
    playGame: function (args, callback) {
        let result = checkArgsLength(args, 1, ['string']);
        if (result !== 'OK') {

            return callback({code: 500, message: result}, [false]);
        }
        database.checkLogin(args[0], function (err, user) {
            if (err) {
                return callback({code: 500, message: err}, [false]);
            }

            database.playGame(user, {type: args[1]}, function (err, result) {
                if (err) {
                    return callback({code: 500, message: err}, [false]);
                }
                callback(null, result);
            })
        });
    },
    getGame: function (args, callback) {
        let result = checkArgsLength(args, 1, ['string']);
        if (result !== 'OK') {

            return callback({code: 500, message: result}, [false]);
        }
        database.checkLogin(args[0], function (err, user) {
            if (err) {
                return callback({code: 500, message: err}, [false]);
            }

            database.getGame(user, {type: args[1]}, function (err, result) {
                if (err) {
                    return callback({code: 500, message: err}, [false]);
                }
                callback(null, result);
            })
        });
    },
    getExchangeCosts: function (args, callback) {
        let result = checkArgsLength(args, 2, ['string', 'object']);
        if (result !== 'OK') {

            return callback({code: 500, message: result}, [false]);
        }
        database.checkLogin(args[0], function (err, user) {
            if (err) {
                return callback({code: 500, message: err}, [false]);
            }

            database.getExchangeCosts(user, args[1], function (err, result) {
                if (err) {
                    return callback({code: 500, message: err}, [false]);
                }
                callback(null, result);
            })
        });
    },
    exchangeLuck: function (args, callback) {
        let result = checkArgsLength(args, 2, ['string', 'string']);
        if (result !== 'OK') {

            return callback({code: 500, message: result}, [false]);
        }
        database.checkLogin(args[0], function (err, user) {
            if (err) {
                return callback({code: 500, message: err}, [false]);
            }

            database.exchangeLuck(user, {type: args[1]}, function (err, result) {
                if (err) {
                    return callback({code: 500, message: err}, [false]);
                }
                callback(null, result);
            })
        });
    },

    feedback: function (args, callback) {
        let result = checkArgsLength(args, 5, ['string', 'string', 'string', 'string', 'string']);
        if (result !== 'OK') {

            return callback({code: 500, message: result}, [false]);
        }
        database.checkLogin(args[0], function (err, user) {
            if (err) {
                return callback({code: 500, message: err}, [false]);
            }

            database.feedback(user, {
                address: args[1],
                contactNum: args[2],
                FIO: args[3],
                postCode: args[4]
            }, function (err, result) {
                if (err) {
                    return callback({code: 500, message: err}, [false]);
                }
                callback(null, result);
            })
        });
    },

    joinLottery: function (args, callback) {
        let result = checkArgsLength(args, 1, ['string']);
        if (result !== 'OK') {

            return callback({code: 500, message: result}, [false]);
        }
        database.checkLogin(args[0], function (err, user) {
            if (err) {
                return callback({code: 500, message: err}, [false]);
            }

            database.joinLottery(user, {type: args[1]}, function (err, result) {
                if (err) {
                    return callback({code: 500, message: err}, [false]);
                }
                callback(null, result);
            })
        });
    },
    getLotteryGameInfo: function (args, callback) {
        let result = checkArgsLength(args, 1, ['string']);
        if (result !== 'OK') {

            return callback({code: 500, message: result}, [false]);
        }
        database.checkLogin(args[0], function (err, user) {
            if (err) {
                return callback({code: 500, message: err}, [false]);
            }

            database.getLotteryGameInfo(user, {type: args[1]}, function (err, result) {
                if (err) {
                    return callback({code: 500, message: err}, [false]);
                }
                callback(null, result);
            })
        });
    },
    getLotteryInfo: function (args, callback) {
        let result = checkArgsLength(args, 1, ['string']);
        if (result !== 'OK') {

            return callback({code: 500, message: result}, [false]);
        }
        database.checkLogin(args[0], function (err, user) {
            if (err) {
                return callback({code: 500, message: err}, [false]);
            }

            database.getLotteryInfo(user, {type: args[1]}, function (err, result) {
                if (err) {
                    return callback({code: 500, message: err}, [false]);
                }
                callback(null, result);
            })
        });
    },

    joinParty: function (args, callback) {
        let result = checkArgsLength(args, 1, ['string']);
        if (result !== 'OK') {

            return callback({code: 500, message: result}, [false]);
        }
        database.checkLogin(args[0], function (err, user) {
            if (err) {
                return callback({code: 500, message: err}, [false]);
            }

            database.joinParty(user, function (err, result) {
                if (err) {
                    return callback({code: 500, message: err}, [false]);
                }
                callback(null, result);
            })
        });
    },
    leaveParty: function (args, callback) {
        let result = checkArgsLength(args, 1, ['string']);
        if (result !== 'OK') {

            return callback({code: 500, message: result}, [false]);
        }
        database.checkLogin(args[0], function (err, user) {
            if (err) {
                return callback({code: 500, message: err}, [false]);
            }

            database.leaveParty(user, function (err, result) {
                if (err) {
                    return callback({code: 500, message: err}, [false]);
                }
                callback(null, result);
            })
        });
    },
    getFreebies: function (args, callback) {
        let result = checkArgsLength(args, 1, ['string']);
        if (result !== 'OK') {

            return callback({code: 500, message: result}, [false]);
        }
        database.checkLogin(args[0], function (err, user) {
            if (err) {
                return callback({code: 500, message: err}, [false]);
            }

            database.getFreebies(user, function (err, result) {
                if (err) {
                    return callback({code: 500, message: err}, [false]);
                }
                callback(null, result);
            })
        });
    },
    getFreebieInfo: function (args, callback) {
        let result = checkArgsLength(args, 2, ['string', 'string']);
        if (result !== 'OK') {

            return callback({code: 500, message: result}, [false]);
        }
        database.checkLogin(args[0], function (err, user) {
            if (err) {
                return callback({code: 500, message: err}, [false]);
            }

            database.getFreebieInfo(user, {
                id: args[1]
            }, function (err, result) {
                if (err) {
                    return callback({code: 500, message: err}, [false]);
                }
                callback(null, result);
            })
        });
    },
    likeDislikeFreebie: function (args, callback) {
        let result = checkArgsLength(args, 3, ['string', 'string', 'boolean']);
        if (result !== 'OK') {

            return callback({code: 500, message: result}, [false]);
        }
        database.checkLogin(args[0], function (err, user) {
            if (err) {
                return callback({code: 500, message: err}, [false]);
            }

            database.likeDislikeFreebie(user, {
                id: args[1],
                like: args[2]
            }, function (err, result) {
                if (err) {
                    return callback({code: 500, message: err}, [false]);
                }
                callback(null, result);
            })
        });
    },
    freebieAction: function (args, callback) {
        let result = checkArgsLength(args, 2, ['string', 'string']);
        if (result !== 'OK') {

            return callback({code: 500, message: result}, [false]);
        }
        database.checkLogin(args[0], function (err, user) {
            if (err) {
                return callback({code: 500, message: err}, [false]);
            }

            database.freebieAction(user, {
                id: args[1]
            }, function (err, result) {
                if (err) {
                    return callback({code: 500, message: err}, [false]);
                }
                callback(null, result);
            })
        });
    },

    getOptionsCost: function (args, callback) {
        let result = checkArgsLength(args, 2, ['string', 'object']);
        if (result !== 'OK') {

            return callback({code: 500, message: result}, [-1]);
        }
        database.checkLogin(args[0], function (err, user) {
            database.getOptionsCost(user, args[1], function (err, result) {
                if (err) {
                    return callback({code: 500, message: err}, [-1]);
                }
                callback(null, result);
            });
        });
    },
    sendPaidMail: function (args, callback) {
        let result = checkArgsLength(args, 2, ['string', 'object']);
        if (result !== 'OK') {

            return callback({code: 500, message: result}, [-1]);
        }
        database.checkLogin(args[0], function (err, user) {
            if (err) {
                return callback({code: 500, message: err}, [-1]);
            }

            database.sendPaidMail(user, args[1], function (err, result) {
                if (err) {
                    return callback({code: 500, message: err}, [-1]);
                }
                callback(null, result);
            })
        });
    },

    getLotteryPrizes: function (args, callback) {
        let result = checkArgsLength(args, 1, ['string']);
        if (result !== 'OK') {

            return callback({code: 500, message: result}, [false]);
        }
        database.checkLogin(args[0], function (err, user) {
            if (err) {
                return callback({code: 500, message: err}, [false]);
            }

            database.getLotteryPrizes(user, function (err, result) {
                if (err) {
                    callback({code: 500, message: err}, [false]);
                } else {
                    callback(null, result);
                }
            })
        });
    },
    setLotteryPrizes: function (args, callback) {
        let result = checkArgsLength(args, 4, ['string', 'string', 'string', 'number']);
        if (result !== 'OK') {

            return callback({code: 500, message: result}, [false]);
        }
        database.checkLogin(args[0], function (err, user) {
            if (err) {
                return callback({code: 500, message: err}, [false]);
            }

            database.setLotteryPrizes(user, {text: args[1], url: args[2], time: args[3]}, function (err, result) {
                if (err) {
                    callback({code: 500, message: err}, [false]);
                } else {
                    callback(null, result);
                }
            })
        });
    },
    activateMoney: function (args, callback) {
        let result = checkArgsLength(args, 2, ['string', 'string']);
        if (result !== 'OK') {

            return callback({code: 500, message: result}, [false]);
        }
        database.checkLogin(args[0], function (err, user) {
            if (err) {
                return callback({code: 500, message: err}, [false]);
            }

            database.activateMoney(user, {id: args[1]}, function (err, parameters) {
                if (err) {
                    callback({code: 500, message: err}, [false]);
                } else {
                    callback(null, parameters);
                }
            })
        });
    },
    getCheckoutForUser: function (args, callback) {
        let result = checkArgsLength(args, 2, ['string', 'string']);
        if (result !== 'OK') {

            return callback({code: 500, message: result}, [-1]);
        }
        database.checkLogin(args[0], function (err, user) {
            if (err) {
                return callback({code: 500, message: err}, [-1]);
            }

            database.getCheckoutForUser(user, {
                id: args[1]
            }, function (err, result) {
                if (err) {
                    return callback({code: 500, message: err}, [-1]);
                }
                callback(null, result);
            })
        });
    },
    getCheckoutInfo: function (args, callback) {
        let result = checkArgsLength(args, 2, ['string', 'number']);
        if (result !== 'OK') {

            return callback({code: 500, message: result}, [-1]);
        }
        database.checkLogin(args[0], function (err, user) {
            if (err) {
                return callback({code: 500, message: err}, [-1]);
            }

            database.getCheckoutInfo(user, {
                checkoutNumber: args[1]
            }, function (err, result) {
                if (err) {
                    return callback({code: 500, message: err}, [-1]);
                }
                callback(null, result);
            })
        });
    },
    getAllCheckouts: function (args, callback) {
        let result = checkArgsLength(args, 1, ['string']);
        if (result !== 'OK') {

            return callback({code: 500, message: result}, [-1]);
        }
        database.checkLogin(args[0], function (err, user) {
            if (err) {
                return callback({code: 500, message: err}, [-1]);
            }

            database.getAllCheckouts(user, function (err, result) {
                if (err) {
                    return callback({code: 500, message: err}, [-1]);
                }
                callback(null, result);
            })
        });
    },
    getMoneyRequests: function (args, callback) {
        let result = checkArgsLength(args, 1, ['string']);
        if (result !== 'OK') {

            return callback({code: 500, message: result}, [false]);
        }
        database.checkLogin(args[0], function (err, user) {
            if (err) {
                return callback({code: 500, message: err}, [false]);
            }

            database.getMoneyRequests(user, function (err, parameters) {
                if (err) {
                    callback({code: 500, message: err}, [false]);
                } else {
                    callback(null, parameters);
                }
            })
        });
    },

    getParameters: function (args, callback) {
        let result = checkArgsLength(args, 2, ['string', 'object']);
        if (result !== 'OK') {

            return callback({code: 500, message: result}, [false]);
        }
        database.checkLogin(args[0], function (err, user) {
            if (err) {
                return callback({code: 500, message: err}, [false]);
            }

            database.getParameters(user, args[2], args[3], args[1], function (err, parameters) {
                if (err) {
                    callback({code: 500, message: err}, [false]);
                } else {
                    callback(null, parameters);
                }
            })
        });
    },
    updateParameters: function (args, callback) {
        let result = checkArgsLength(args, 3, ['string', 'object', 'object']);
        if (result !== 'OK') {

            return callback({code: 500, message: result}, [false]);
        }
        database.checkLogin(args[0], function (err, user) {
            if (err) {
                return callback({code: 500, message: err}, [false]);
            }

            database.updateParameters(user, args[3], args, function (err, parameters) {
                if (err) {
                    callback({code: 500, message: err}, [false]);
                } else {
                    callback(null, parameters);
                }
            })
        });
    },

    getVersion: function (args, callback) {
        callback(null, [VERSION_ID]);
    },
    setSuperType: function (args, callback) {
        let result = checkArgsLength(args, 3, ['string', 'string', 'string']);
        if (result !== 'OK') {

            return callback({code: 500, message: result}, [false]);
        }
        database.checkLogin(args[0], function (err, user) {
            if (err) {
                return callback({code: 500, message: err}, [false]);
            }

            database.setSuperType(user, {
                id: args[1],
                type: args[2]
            }, function (err, result) {
                if (err) {
                    return callback({code: 500, message: err}, [false]);
                }
                callback(null, result);
            })
        });
    },
    activateOptions: function (args, callback) {
        let result = checkArgsLength(args, 2, ['string', 'number']);
        if (result !== 'OK') {

            return callback({code: 500, message: result}, [-1]);
        }
        database.checkLogin(args[0], function (err, user) {
            if (err) {
                return callback({code: 500, message: err}, [-1]);
            }

            database.activateOptions(user, {
                checkoutNumber: args[1]
            }, function (err, result) {
                if (err) {
                    return callback({code: 500, message: err}, [-1]);
                }
                callback(null, result);
            })
        });
    },
    getAllWinners: function (args, callback) {
        let result = checkArgsLength(args, 1, ['string']);
        if (result !== 'OK') {

            return callback({code: 500, message: result}, [false]);
        }
        database.checkLogin(args[0], function (err, user) {
            if (err) {
                return callback({code: 500, message: err}, [false]);
            }

            database.getAllWinners(user, function (err, result) {
                if (err) {
                    return callback({code: 500, message: err}, [false]);
                }
                callback(null, result);
            })
        });
    },
    shareMoney: function (args, callback) {
        let result = checkArgsLength(args, 1, ['string', 'string', 'number']);
        if (result !== 'OK') {

            return callback({code: 500, message: result}, [false]);
        }
        database.checkLogin(args[0], function (err, user) {
            if (err) {
                return callback({code: 500, message: err}, [false]);
            }

            database.shareMoney(user, {type: args[1], value: args[2]}, function (err, result) {
                if (err) {
                    return callback({code: 500, message: err}, [false]);
                }
                callback(null, result);
            })
        });
    },

    moneyRequest: function (args, callback) {
        let result = checkArgsLength(args, 1, ['string']);
        if (result !== 'OK') {

            return callback({code: 500, message: result}, [false]);
        }
        database.checkLogin(args[0], function (err, user) {
            if (err) {
                return callback({code: 500, message: err}, [false]);
            }

            database.moneyRequest(user, function (err, parameters) {
                if (err) {
                    callback({code: 500, message: err}, [false]);
                } else {
                    callback(null, parameters);
                }
            })
        });
    },
    getMoneyRequestInfo: function (args, callback) {
        let result = checkArgsLength(args, 2, ['string', 'string']);
        if (result !== 'OK') {

            return callback({code: 500, message: result}, [false]);
        }
        database.checkLogin(args[0], function (err, user) {
            if (err) {
                return callback({code: 500, message: err}, [false]);
            }

            database.getMoneyRequestInfo(user, {requestId: args[1]}, function (err, parameters) {
                if (err) {
                    callback({code: 500, message: err}, [false]);
                } else {
                    callback(null, parameters);
                }
            })
        });
    },

    uploadImage: function (args, callback) {
        let result = checkArgsLength(args, 1, ['string']);
        if (result !== 'OK') {

            return callback({code: 500, message: result}, [false]);
        }
        database.checkLogin(args[0], function (err, user) {
            if (err) {
                return callback({code: 500, message: err}, [false]);
            }

            database.uploadImage(user, function (err, parameters) {
                if (err) {
                    callback({code: 500, message: err}, [false]);
                } else {
                    callback(null, parameters);
                }
            })
        });
    },
    removeImage: function (args, callback) {
        let result = checkArgsLength(args, 2, ['string', 'string']);
        if (result !== 'OK') {

            return callback({code: 500, message: result}, [false]);
        }
        database.removeImage(args[1], function (err, parameters) {
            if (err) {
                callback({code: 500, message: err}, [false]);
            } else {
                callback(null, parameters);
            }
        });
    },
    getInfo: function (args, callback) {
        let result = checkArgsLength(args, 1, ['string']);
        if (result !== 'OK') {

            return callback({code: 500, message: result}, [""]);
        }
        database.checkLogin(args[0], function (err, user) {
            if (err) {
                return callback({code: 500, message: err}, [""]);
            }

            database.getInfo(user, args, function (err, parameters) {
                if (err) {
                    callback({code: 500, message: err}, [""]);
                } else {
                    callback(null, parameters);
                }
            });
        });
    },
    infoUpdate: function (args, callback) {
        let result = checkArgsLength(args, 1, ['string']);
        if (result !== 'OK') {

            return callback({code: 500, message: result}, [false]);
        }
        database.checkLogin(args[0], function (err, user) {
            if (err) {
                return callback({code: 500, message: err}, [false]);
            }

            database.infoUpdate(user, args[1], function (err, success) {
                if (err) {
                    callback({code: 500, message: err}, [false]);
                } else {
                    callback(null, success);
                }
            })
        });
    }
});
let checkArgsLength = function (args, needLength, types) {
    if (!args || args[0] === undefined) {
        return 'Нет токена';
    }
    if(args.length < needLength){
        return 'Недостаточно аргументов';
    } else {
        let fillArg = true;
        args.forEach(function (arg, i) {
            if(arg === undefined || (types[i] && typeof arg !== types[i])){
                fillArg = false;
            }
        });
        if(!fillArg){
            return 'Заполнены не все поля';
        }
    }
    return 'OK';
};

server.http().listen(config.port, function() {
    log.log('info', 'GroovyHamster listening on port ' + config.port);
});
let credentials = {key: fs.readFileSync('keys/server.key', 'utf8'), cert: fs.readFileSync('keys/server.crt', 'utf8')};
server.https(credentials).listen(config.portSSL, function() {
    log.log('info', 'GroovyHamster https listening on port ' + config.portSSL);
});
