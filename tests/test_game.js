const jayson = require('jayson');
const client = jayson.client.http({port: 1337});

const mongoose = require('mongoose');
const options = {promiseLibrary: require('bluebird')};
mongoose.connect(require('../config.json').mongoose.uri, options);
mongoose.connection.on('open', function () {
    console.log('Connected to database!');
});
let User;
require('../schemas/user')(mongoose, function (user) {
    User = user;

    User.find({}).skip(0).limit(20).exec(function (err, users) {
        users.forEach(function (user) {
            client.request('login', ["", user.accountID, user.password], function (err, response) {
                if (err) {
                    throw(err);
                }
                // client.request('getInfo', [response.result[0], "events"], function (err, response) {
                //     if (err) {
                //         throw(err);
                //     }
                //     console.log(response)
                // });
                if (response.result && response.result[0]) {
                    for (let i = 0; i < 15; i++) {
                        client.request('playGame', [response.result[0]], function (err, response) {
                            if (err) {
                                throw(err);
                            }
                            console.log(response);
                        });
                    }
                }
            });
        });
    });
});