var jayson = require('jayson');
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
var client = jayson.client.https('https://localhost:1338');
// var client = jayson.client.https('https://ghclub.net:1338');

var registerAdministrator = function () {
    client.request('sendInvite', ["", "administrator", "secretKey", "", "verySecretKeyForAdminRegister"], function(err, response) {
        if (err) {
            throw(err);
        }
        console.log(response);
        if(response.error) {console.log(response.error); return;}
        var invite = response.result[0];
        client.request('checkInvite', ["", invite], function(err, response) {
            if (err) {
                throw(err);
            }
            console.log(response);
            if(response.error) {console.log(response.error); return;}
            var accountID = response.result[1];
            client.request('register', ["", response.result[2], accountID, invite, "Россия", "Омск", "jednike@gmail.com", "password"], function(err, response) {
                if (err) throw(err);
                console.log(response);
            });
        });
    });
};
registerAdministrator();