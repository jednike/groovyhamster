var jayson = require('jayson');
var client = jayson.client.http({port: 1337});

var mongoose = require('mongoose');
var options = {promiseLibrary: require('bluebird')};
mongoose.connect('mongodb://groovyAdmin:AsQmAGH@localhost/GroovyHamster', options, function (err) {
    if(err){
        throw err;
    }
});

mongoose.connection.on('open', function () {
    console.log('Connected to database!');
    var User;
    require('../schemas/user')(mongoose, function (user, UserSchema) {
        User = user;
        var Administrator = require('../schemas/administrator')(mongoose, UserSchema);
        var Advertiser = require('../schemas/advertiser')(mongoose, UserSchema);
        var Finance = require('../schemas/finance')(mongoose);
        User.remove({accoundID: 'a8689v15242'}, function(err){
            if(err){
                console.log(err);
            }
        });
        User.remove({accountID:'a4448d17021'}, function(err) { if(err) console.log(err); });
        User.find().populate('referrers.user').exec(function (err, users) {
            console.log(users.length);
            users.forEach(function (user) {
                if(user.accountID) {
                    console.log(user.accountID);
                    var tempAwards = [];
                    user.awards.forEach(function (award, i, awards) {
                        var exist = false;
                        tempAwards.forEach(function (tempAward) {
                            if (tempAward.companyId === award.companyId) {
                                return exist = true;
                            }
                            if (exist) {
                                console.log('Delete repeat award');
                                awards.splice(i, 1);
                            } else {
                                tempAwards.push(award);
                            }
                        });
                        user.referrers.user.forEach(function (ref, i, refs) {
                            console.log(ref.accountID);
                            if (ref.star > 0) {
                                user.Re += 10;
                                console.log('Delete with star');
                                refs.splice(i, 1);
                            } else if (ref._type === 'advertiser' || ref._type === 'administrator') {
                                console.log('Delete another type');
                                refs.splice(i, 1);
                            }
                        });
                        user.save(function (err) {
                            if (err) {
                                console.log(err);
                            }
                        });
                    });
                } else {
                    user.remove(function(err){
                        if(err){
                            console.log(err);
                        }
                    });
                }
            });
        });
        User.find().populate('referrers.advertiser.companyId').exec(function (err, users) {
            users.forEach(function (user) {
                user.referrers.advertiser.forEach(function (ref, i, refs) {
                    if(!ref.companyId || !ref.companyId.accountID){
                        refs.splice(i, 1);
                        console.log('Delete user from referrers');
                    }
                });
                user.save(function (err) {
                    if (err) {
                        console.log(err);
                    }
                });
            });
        });
        User.find().populate('referrers.awards.companyId').exec(function (err, users) {
            users.forEach(function (user) {
                user.awards.forEach(function (award, i, awards) {
                    if(!award.companyId || !award.companyId.accountID){
                        awards.splice(i, 1);
                        console.log('Delete user from award');
                    }
                });
                user.save(function (err) {
                    if (err) {
                        console.log(err);
                    }
                });
            });
        });
        User.find().populate('favorites').exec(function (err, users) {
            users.forEach(function (user) {
                user.favorites.forEach(function (award, i, awards) {
                    if(!award || !award.accountID){
                        awards.splice(i, 1);
                        console.log('Delete user from favorites');
                    }
                });
                user.save(function (err) {
                    if (err) {
                        console.log(err);
                    }
                });
            });
        });
    });
});
